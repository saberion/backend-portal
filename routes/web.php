<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEnd\HomeController@index');
Route::view('faq', 'frontend.faq');
Route::view('about', 'frontend.about');
Route::view('media', 'frontend.media');
Route::get('competition-one', 'FrontEnd\CompetitionOneController@index');
Route::get('competition-two', 'FrontEnd\CompetitionTwoController@index');

Route::get('login', 'RootController@index')->name('root')->middleware('guest');

Route::group([
    'as'            => 'students.',
    'prefix'        => 'students',
    'namespace'     => 'Students',
    'middleware'    => 'guest'
], function () {
    Route::get('register', 'RegisterController@showRegistrationForm')
        ->name('register');
    Route::post('register', 'RegisterController@register');

    Route::post('login', 'LoginController@login')->name('login');
});

Route::group([
    'as'            => 'students.',
    'prefix'        => 'students',
    'namespace'     => 'Students'
], function () {
    Route::get('email/verify/{id}', 'VerificationController@verify')
        ->middleware('signed', 'throttle:6,1')
        ->name('verification.verify');
    Route::post('logout', 'LoginController@logout')->name('logout');
});

Route::group([
    'as'            => 'students.profile.',
    'prefix'        => 'students/profile',
    'namespace'     => 'Students',
    'middleware'    => 'auth:student'
], function () {
    Route::get('', 'ProfileController@index')->name('index');
    Route::get('edit', 'ProfileController@edit')->name('edit');
    Route::patch('', 'ProfileController@update')->name('update');
});

Route::group([
    'as'            => 'students.submission.',
    'prefix'        => 'students/submission',
    'namespace'     => 'Students',
    'middleware'    => 'auth:student'
], function () {
    Route::get('', 'SubmissionController@index')->name('index');
    Route::get('reviews', 'SubmissionController@reviews')->name('reviews');
    Route::get('success', 'SubmissionController@success')->name('success');
    Route::post('', 'SubmissionController@store')->name('store');
});

Route::group([
    'as'            => 'students.password.',
    'prefix'        => 'students/password',
    'namespace'     => 'Students'
], function () {
    Route::get('reset', 'ForgotPasswordController@showLinkRequestForm')
        ->name('request');
    Route::post('email', 'ForgotPasswordController@sendResetLinkEmail')
        ->name('email');
    Route::get('reset/{id}', 'ResetPasswordController@showResetForm')
        ->middleware('signed', 'throttle:6,1')
        ->name('reset');
    Route::post('reset', 'ResetPasswordController@reset')->name('update');
});

Route::group([
    'as'            => 'juryMembers.',
    'prefix'        => 'jury-members',
    'namespace'     => 'JuryMembers',
    'middleware'    => 'guest'
], function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
});

Route::post('jury-members/logout', 'JuryMembers\LoginController@logout')
    ->name('juryMembers.logout');

Route::group([
    'as'            => 'juryMembers.',
    'prefix'        => 'jury-members',
    'namespace'     => 'JuryMembers',
    'middleware'    => 'auth:juryMember'
], function () {
    Route::get('dashboard', 'DashboardController@index')
        ->name('dashboard.index');
    Route::get('review/{id}', 'ReviewController@create')
        ->name('review.create');
    Route::post('review/{id}', 'ReviewController@store');
});

Route::group([
    'as'            => 'admin.',
    'prefix'        => 'admin',
    'namespace'     => 'Admin',
    'middleware'    => 'guest'
], function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
});

Route::group([
    'as'            => 'admin.dashboard.',
    'prefix'        => 'admin/dashboard',
    'namespace'     => 'Admin',
    'middleware'    => 'auth:admin'
], function () {
    Route::get('', 'DashboardController@index')->name('index');
    Route::delete('delete-entry/{id}', 'DashboardController@deleteEntry')
        ->name('deleteEntry');
});

Route::get('admin/entries/{id}/students', 'Admin\EntryController@students')
    ->name('admin.entries.students')->middleware('auth:admin');

Route::get('admin/reviews', 'Admin\ReviewController@index')
    ->name('admin.reviews.index')->middleware('auth:admin');

Route::group([
    'as'            => 'admin.juryMembers.',
    'prefix'        => 'admin/jury-members',
    'namespace'     => 'Admin',
    'middleware'    => 'auth:admin'
], function () {
    Route::get('', 'JuryMemberController@index')->name('index');
    Route::post('', 'JuryMemberController@store')->name('store');
    Route::patch('{id}', 'JuryMemberController@update')->name('update');
    Route::delete('{id}', 'JuryMemberController@destroy')->name('destroy');
});

Route::group([
    'as'            => 'admin.competitionLock.',
    'prefix'        => 'admin/competition-lock',
    'namespace'     => 'Admin',
    'middleware'    => 'auth:admin'
], function () {
    Route::get('', 'CompetitionLockController@index')->name('index');
    Route::get('results/{competition}', 'CompetitionLockController@results')
        ->name('results');
    Route::post('{competition}', 'CompetitionLockController@store')
        ->name('store');
});

Route::post('admin/logout', 'Admin\LoginController@logout')
    ->name('admin.logout');