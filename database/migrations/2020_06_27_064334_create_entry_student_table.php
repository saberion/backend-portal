<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntryStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entry_student', function (Blueprint $table) {
            $table->foreignId('student_id')->constrained();
            $table->foreignId('entry_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entry_student', function (Blueprint $table) {
            $table->dropForeign(['student_id', 'entry_id']);
        });

        Schema::dropIfExists('entry_student');
    }
}
