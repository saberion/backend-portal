<div class="back-section">
    <a href="{{ $url ?? '#' }}">
        <img src="{{ asset('images/left-arrow.svg') }}" alt="">
        <span>Back</span>
    </a>
</div>