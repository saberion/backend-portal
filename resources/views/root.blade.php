@extends('layouts.app')

@section('title', 'Student Login')

@section('content')
    <div class="main-wrapper">
        <div class="inner-wrapper">
            <div class="top-part">
                @include('partials.head', [
                    'title' => 'SUBMISSION PORTAL',
                    'description' => 'In order to submit your work, you have to be a registered user.'
                ])

                <h2>ALREADY REGISTERED? LOGIN NOW</h2>

                <form action="{{ route('students.login') }}" method="POST">
                    @csrf

                    <div class="email-sec">
                        <div class="input-field">
                            <input type="text" name="username" value="{{ old('username') }}" id="Competitior_id"
                                class="validate" minlength="7" required>
                            <label for="Competitior_id">Competitior ID</label>

                            @error('username')
                                <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="contain-inputs">
                        <div class="input-cover left-section">
                            <div class="input-field">
                                <input type="email" name="email" value="{{ old('email') }}" id="email_feild" class="validate"
                                    autocomplete="email" required>
                                <label for="email_feild">Email</label>

                                @error('email')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="password" name="password" id="password" class="validate"
                                    autocomplete="current-password" minlength="8" required>
                                <label for="password">Password</label>

                                @error('password')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="wrap-login-btn">
                            <button type="submit" class="waves-effect waves-light btn">Login</button>
                            <a href="{{ route('students.password.request') }}">Reset Password?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <section class="bottom-part">
        <h2>NEW HERE? GET REGISTERED NOW</h2>

        <div class="input-section">
            <span class="help-label">Select your entry type <img src="{{ asset('images/question.svg') }}" alt=""></span>

            <div class="inner-wrapper-bottom">
                <form action="{{ route('students.register') }}" method="GET">
                    <div class="register-form">
                        <div class="input-radio">
                            <label>
                                <input type="radio" name="type" value="individual">
                                <span>Individual Entry</span>
                            </label>
                        </div>

                        <div class="input-radio">
                            <label>
                                <input type="radio" name="type" value="group">
                                <span>Group Entry</span>
                            </label>
                        </div>

                        <button class="waves-effect waves-light btn">Proceed</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    @include('layouts.alerts')
@endsection