@php
    $route = Route::currentRouteName();
@endphp

<section id="sidebar">
    <div class="logo-sec">
        <img src="{{ asset('images/logo.svg') }}" alt="">

        <span class="text-uppercase">PCC STUDENT DESIGN</span>

        <span class="text-uppercase">SUBMISSION PORTAL</span>
    </div>

    @auth('student')
        <div class="page-link">
            <ul>
                <li><a class="{{ Str::contains($route, 'submission') ? 'active' : '' }}"
                    href="{{ route('students.submission.index') }}">COMPETITION</a></li>

                <li><a class="{{ Str::contains($route, 'profile') ? 'active' : '' }}"
                    href="{{ route('students.profile.index') }}">PROFILE</a></li>
            </ul>
        </div>
    @endauth

    @auth('juryMember')
        <div class="page-link">
            <ul>
                <li><a class="{{ request('competition') == 1 ? 'active' : '' }}"
                    href="{{ route('juryMembers.dashboard.index', ['competition' => 1]) }}">INTERACTIVE WALL
                </a></li>
                <li><a class="{{ request('competition') == 2 ? 'active' : '' }}"
                    href="{{ route('juryMembers.dashboard.index', ['competition' => 2]) }}">BEACH SCULPTURE</a></li>
            </ul>
        </div>
    @endauth

    @auth('admin')
        <div class="page-link">
            <ul>
                <li><a class="{{ Str::contains($route, 'dashboard') ? 'active' : '' }}"
                    href="{{ route('admin.dashboard.index') }}">ENTRANTS</a></li>

                <li><a class="{{ Str::contains($route, 'reviews') ? 'active' : '' }}"
                    href="{{ route('admin.reviews.index') }}">ALL SCORES</a></li>

                <li><a class="{{ Str::contains($route, 'juryMembers') ? 'active' : '' }}"
                    href="{{ route('admin.juryMembers.index') }}">JURY</a></li>

                <li><a class="{{ Str::contains($route, 'competitionLock') ? 'active' : '' }}"
                    href="{{ route('admin.competitionLock.index') }}">COMPETITION LOCK</a></li>
            </ul>
        </div>
    @endauth

    <div class="portlogo">
        <img src="{{ asset('images/portcityImage.png') }}" alt="">
    </div>
</section>