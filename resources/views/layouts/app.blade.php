<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />

    <title>Envision The Future | @yield('title')</title>

    <link rel="stylesheet" href="{{  asset('css/materialize.min.css') }}">
    <link rel="stylesheet" href="{{  mix('css/app.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
</head>

<body>
    <div class="main-container">
        @include('layouts.sidebar')

        <main id="main-section" role="main">
            @yield('content')

            @include('layouts.footer')
        </main>
    </div>

    <script src="{{ asset('js/jquery.slim.min.js') }}"></script>
    <script src="{{ asset('js/materialize.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

    @stack('scripts')
</body>

</html>