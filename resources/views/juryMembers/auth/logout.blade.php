<div class="login">
    <div class="logout">
        <form action="{{ route('juryMembers.logout') }}" method="POST">
            @csrf

            <button type="submit">Logout</button>
        </form>
    </div>
</div>