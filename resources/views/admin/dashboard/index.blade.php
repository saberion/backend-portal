@extends('layouts.app')

@push('scripts')
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
@endpush

@section('title', 'Admin Dashboard')

@section('content')
    <div class="main-wrapper">
        @include('admin.auth.logout')

        <div class="spacer">
            @include('partials.head', ['title' => 'COMPETITION'])
        </div>

        <div style="width: 100%" class="dashboard">
            <div class="table-title">
                <h2 style="padding-bottom: 12px;">ENTRANTS</h2>
            </div>

            <div class="pagination-sec">
                <div class="record">
                    <span>{{ $entries->total() }}</span> <span>records found.</span>
                </div>

                <div class="pagination">
                    @if (request()->filled('competition'))
                        {{ $entries->appends(['competition' => request('competition')])->links() }}
                    @else
                        {{ $entries->links() }}
                    @endif
                </div>
            </div>

            <form action="{{ route('admin.dashboard.index') }}" method="GET">
                <div class="table-top-bar">
                    <div class="one-side">
                        <label style="padding-left: 15px">
                            <input type="radio" name="competition" value="1"
                                {{ request('competition') == 1 ? 'checked' : '' }} required>
                            <span class="table-top-rd-fnt">INTERACTIVE WALL – VITALITY</span>
                        </label>
                    </div>

                    <div class="one-side" style="padding-left: 70px">
                        <label style="padding-left: 15px">
                            <input type="radio" name="competition" value="2"
                                {{ request('competition') == 2 ? 'checked' : '' }} required>
                            <span class="table-top-rd-fnt">BEACH SCULPTURE – DREAM</span>
                        </label>
                    </div>

                    <div class="one-side" style="padding-left: 70px">
                        <label style="padding-left: 15px">
                            <input type="radio" name="competition" value="0"
                                {{ request('competition') == 0 ? 'checked' : '' }} required>
                            <span class="table-top-rd-fnt">ALL</span>
                        </label>
                    </div>

                    <div class="one-side" style="padding-left: 70px">
                        <button style="width: 200px" type="submit" class="waves-effect waves-light btn">Filter</button>
                    </div>
                </div>
            </form>

            <table class="striped highlight responsive-table">
                <thead>
                    <tr>
                        <th>Account ID</th>
                        <th>Account Type</th>
                        <th>Interactive Wall Submitted?</th>
                        <th>Beach Sculpture Submitted?</th>
                        <th>Created</th>
                    </tr>
                </thead>

                <tbody>
                    @if ($entries->isNotEmpty())
                        @foreach ($entries as $entry)
                            <tr>
                                <td>{{ $entry->identity }}</td>

                                <td>
                                    <a href="{{ route('admin.entries.students', $entry->id) }}">
                                        {{ ucfirst($entry->type) }}
                                    </a>
                                </td>

                                <td>{{ $entry->competition_one_count ? 'Yes' : 'No' }}</td>

                                <td>{{ $entry->competition_two_count ? 'Yes' : 'No' }}</td>

                                <td>{{ $entry->created_at->diffForHumans() }}</td>

                                <td>
                                    <form id="deleteEntry{{ $entry->id }}"
                                        action="{{ route('admin.dashboard.deleteEntry', $entry->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')

                                        <button type="button" class="delete-entry">
                                            <img src="{{ asset('images/delete.png') }}">
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>

    @include('layouts.alerts')
@endsection

@push('scripts')
    <script>
        $('.delete-entry').click(function () {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    var deleteFormId = $(this).parent().attr('id');

                    $('#' + deleteFormId).submit();
                }
            })
        });
    </script>
@endpush
