@php
    $avatar = ! empty($student->avatar) ? Storage::url($student->avatar)
        : asset('images/profile-image01.png');
@endphp

<div class="profile-component">
    <div class="profile-card">
        <div class="wrap-back">
            <img src="{{ $avatar }}" alt="{{ $student->name }}'s Profile Picture">
        </div>

        <div class="profile-title-section">
            <h5>{{ $student->name }}</h5>
            <h5>{{ $entry->identity }}</h5>
        </div>
    </div>

    <div class="wrap-input-ele profile-spaceing">
        <div class="lft-sec">
            <div class="profile-iteam">
                <p class="profile-title">NIC</p>
                <p class="profile-head">{{ $student->nic }}</p>
            </div>

            <div class="profile-iteam">
                <p class="profile-title">Email Address</p>
                <p class="profile-head">{{ $student->email }}</p>
            </div>

            <div class="profile-iteam">
                <p class="profile-title">Contact Number</p>
                <p class="profile-head">{{ $student->contact }}</p>
            </div>
        </div>

        <div class="lft-sec">
            <div class="profile-iteam">
                <p class="profile-title">University</p>
                <p class="profile-head">{{ $student->university }}</p>
            </div>

            <div class="profile-iteam">
                <p class="profile-title">University Reg. No</p>
                <p class="profile-head">{{ $student->registration }}</p>
            </div>

            <div class="profile-iteam">
                <div class="profile-iteam">
                    <p class="profile-title">Course Following</p>
                    <p class="profile-head">{{ $student->course }}</p>
                </div>
            </div>
        </div>

        <div class="lft-sec">
            <div class="profile-iteam">
                <p class="profile-title">Year of Study</p>
                <p class="profile-head">{{ $student->year }}</p>
            </div>
        </div>
    </div>
</div>