@extends('layouts.app')

@section('title', 'All Scores')

@section('content')
    <div class="main-wrapper">
        @include('admin.auth.logout')

        <div class="full-contact-container-three">
            <div class="spacer">
                @include('partials.head', ['title' => 'COMPETITION'])
            </div>

            <div style="width: 100%" class="dashboard">
                <div class="table-title">
                    <h2>ALL SCORES</h2>
                </div>

                <div class="pagination-sec">
                    <div class="record">
                        <span>{{ $results->total() }}</span> <span>records found.</span>
                    </div>
    
                    <div class="pagination">
                        @if (request()->filled('competition'))
                            {{ $results->appends(['competition' => request('competition')])->links() }}
                        @else
                            {{ $results->links() }}
                        @endif
                    </div>
                </div>

                <form action="{{ route('admin.reviews.index') }}" method="GET">
                    <div class="table-top-bar">
                        <div class="one-side">
                            <label style="padding-left: 15px">
                                <input type="radio" name="competition" value="1"
                                    {{ request('competition') == 1 ? 'checked' : '' }} required>
                                <span class="table-top-rd-fnt">INTERACTIVE WALL – VITALITY</span>
                            </label>
                        </div>
    
                        <div class="one-side" style="padding-left: 70px">
                            <label style="padding-left: 15px">
                                <input type="radio" name="competition" value="2"
                                    {{ request('competition') == 2 ? 'checked' : '' }} required>
                                <span class="table-top-rd-fnt">BEACH SCULPTURE – DREAM</span>
                            </label>
                        </div>
    
                        <div class="one-side" style="padding-left: 70px">
                            <button style="width: 200px" type="submit" class="waves-effect waves-light btn">Filter</button>
                        </div>
                    </div>
                </form>

                <table class="striped highlight responsive-table">
                    <thead>
                        <tr>
                            <th>Account ID</th>
                            <th>Jury Member</th>
                            <th>Points</th>
                            <th>Proposal</th>
                            <th>Design</th>
                            <th>Comments</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if ($results->isNotEmpty())
                            @foreach ($results as $result)
                                <tr>
                                    <td class="primary-color">{{ $result->identity }}</td>

                                    <td>{{ $result->name }}</td>

                                    <td>{{ $result->points }}</td>

                                    <td class="pdf-view">
                                        <a href="{{ Storage::url($result->proposal) }}" class="pdf"
                                            target="_blank">
                                            <img src="{{ asset('images/pdf.png') }}" alt="PDF Icon"> View
                                        </a>
                                    </td>

                                    <td class="pdf-view">
                                        <a href="{{ Storage::url($result->design) }}" class="pdf"
                                            target="_blank">
                                            <img src="{{ asset('images/pdf.png') }}" alt="PDF Icon"> View
                                        </a>
                                    </td>

                                    <td>
                                        <a
                                            class="modal-trigger"
                                            href="#comment{{ $loop->iteration }}">View</a>
                                    </td>
                                </tr>

                                <div id="comment{{ $loop->iteration }}" class="modal">
                                    <div class="modal-content">
                                        <p>{{ $result->comments }}</p>
                                    </div>

                                    <div class="modal-footer">
                                        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('.modal').modal();
    </script>
@endpush