@extends('layouts.app')

@section('title', 'Jury Members')

@section('content')
    <div class="main-wrapper">
        @include('admin.auth.logout')

        <div class="spacer jury-sp">
            @include('partials.head', ['title' => 'ADMIN DASHBOARD'])
        </div>

        <div style="width: 100%" class="dashboard jury-dash">
            <div class="table-title">
                <h2>JURY</h2>
            </div>

            <table class="striped highlight responsive-table">
                <thead>
                    <tr>
                        <th>Jury Member Name</th>
                        <th>Email</th>
                        <th>Created</th>
                        <th>Total Reviewed</th>
                        <th>Total Assigned</th>
                    </tr>
                </thead>

                <tbody>
                    @if ($juryMembers->isNotEmpty())
                        @foreach ($juryMembers as $juryMember)
                            <tr>
                                <td class="primary-color">{{ $juryMember->name }}</td>
                                <td>{{ $juryMember->email }}</td>
                                <td>{{ $juryMember->created_at->diffForHumans() }}</td>
                                <td>{{ $juryMember->reviews_count }}</td>
                                <td>{{ $juryMember->trashed() ? 0 : $submissionsCount }}</td>

                                @if ($juryMember->trashed())
                                    <td>
                                        <form action="{{ route('admin.juryMembers.update', $juryMember->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('PATCH')

                                            <button class="btn waves-effect waves-light" type="submit">Mark As Active
                                                </button>
                                        </form>
                                    </td>
                                @else
                                    <td>
                                        <form action="{{ route('admin.juryMembers.destroy', $juryMember->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')

                                            <button class="btn waves-effect waves-light" type="submit">Mark As Inactive
                                                </button>
                                        </form>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>

            <button data-target="modaljury" class="waves-effect waves-light btn jury-add-btn modal-trigger">
                ADD JURY MEMBERS</button>

            <div id="modaljury" class="modal jury-model">
                <div class="modal-content">
                    <h4>Add Jury Member</h4>

                    <div class="input-wrap">
                        <form action="{{ route('admin.juryMembers.store') }}" method="POST">
                            @csrf

                            <div class="input-field">
                                <input type="text" name="name" value="{{ old('name') }}" id="name" class="validate"
                                    autocomplete="name" required>
                                <label for="name">Name *</label>

                                @error('name')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="email" name="email" value="{{ old('email') }}" id="email" class="validate"
                                    autocomplete="email" required>
                                <label for="email">Email Address *</label>

                                @error('email')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="password" name="password" id="password" class="validate"
                                    autocomplete="new-password" required>
                                <label for="password" class="">Password *</label>
                            </div>

                            <div class="input-field">
                                <input type="password" name="password_confirmation" id="passwordConfirmation" class="validate"
                                    autocomplete="new-password" required>
                                <label for="passwordConfirmation" class="">Confirm Password *</label>
                            </div>
    
                            <button type="submit" style="margin-top: 25px" class="waves-effect waves-light btn">ADD
                                JURY MEMBERS</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.alerts')
@endsection

@push('scripts')
    <script>
        $('.modal').modal();
    </script>
@endpush