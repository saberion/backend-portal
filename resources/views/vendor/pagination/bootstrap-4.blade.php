@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="page-link left-arrow" aria-hidden="true">
                    <img src="{{ asset('images/leftt-arrow.svg') }}" alt="">
                </span>
            </li>
        @else
            <li class="page-item">
                <a class="page-link left-arrow" href="{{ $paginator->previousPageUrl() }}" rel="prev"
                    aria-label="@lang('pagination.previous')">
                    <img src="{{ asset('images/leftt-arrow.svg') }}" alt="">
                </a>
            </li>
        @endif

        <li class="page-item active" aria-current="page">
            <span class="page-link">{{ $paginator->firstItem() }} - {{ $paginator->lastItem() }}</span>
        </li>

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link right-arrow" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
                    <img src="{{ asset('images/right-arrow.svg') }}" alt="">
                </a>
            </li>
        @else
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="page-link right-arrow" aria-hidden="true">
                    <img src="{{ asset('images/right-arrow.svg') }}" alt="">
                </span>
            </li>
        @endif
    </ul>
@endif
