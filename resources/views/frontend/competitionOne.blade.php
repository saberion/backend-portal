<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width,initial-scale=1">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Envision</title>
   <link rel="stylesheet" href="/assets/css/lib.css">
   <link rel="stylesheet" href="/assets/css/main.css">
   <link rel="shortcut icon" href="/assets/images/">
   <link rel="stylesheet" href="{{ mix('css/competitions.css') }}">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js"></script>
   
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async="async" src="https://www.googletagmanager.com/gtag/js?id=UA-174141483-1"></script>
   <script>
      window.dataLayer = window.dataLayer || [];
      function gtag() {
         dataLayer.push(arguments);
      }
      gtag('js', new Date());

      gtag('config', 'UA-174141483-1');
   </script>

</head>

<body class="onload aod-competition">
   <div class="left-menu">
      <div class="logo">
         <a href="/">
            <img src="/assets/images/navigations/logo.svg" alt="">
         </a>
      </div>
      <div class="pcc-sec lg1">
         <span>PCC STUDENT DESIGN COMPETITION 2020</span>
      </div>
      <div class="social-media">
         <div class="text-ani-inner">
            <a class="lgz3" href="https://www.facebook.com/PortCityLK" target="_blank">
               <img src="/assets/images/navigations/left-icon-01.svg" alt="">
            </a>
         </div>

         <div class="text-ani-inner" target="_blank">
            <a class="lgz3" href="https://www.youtube.com/channel/UCu-RLmtH6Tm1Y_AbmvFuFog/featured">
               <img src="/assets/images/navigations/left-icon-02.png" alt="">
            </a>
         </div>
         <div class="text-ani-inner" target="_blank">
            <a class="lgz3" href="https://www.instagram.com/portcitycolombo/">
               <img src="/assets/images/navigations/left-icon-03.svg" alt="">
            </a>
         </div>
         <div class="text-ani-inner" target="_blank">
            <a class="lgz3" href="https://www.linkedin.com/company/port-city-colombo/">
               <img src="/assets/images/navigations/left-icon-04.svg" alt="">
            </a>
         </div>
         <div class="text-ani-inner" target="_blank">
            <a class="lgz3" href="https://twitter.com/PortCityColombo">
               <img src="/assets/images/navigations/left-icon-05.svg" alt="">
            </a>
         </div>

      </div>
   </div>

   <div class="menu-wrapper">
      <div class="menu">
         <div class="data">
            <ul>
               <li><a class="nav-data" href="/">HOME</a></li>
               <li><a class="nav-data" href="/about">ABOUT PCC</a></li>
               <li><a class="nav-data" href="/competition-one">THE COMPETITION</a></li>
               <li><a class="nav-data" href="/media">MEDIA</a></li>
               <li><a class="nav-data" href="/faq">FAQ</a></li>
            </ul>
         </div>
      </div>
   </div>

   <div id="loader-wrapper">
      <div id="loader">
         <img src="/assets/images/navigations/logo.svg" alt="">
      </div>

      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>

   </div>






   <div class="mobile-navigation-wrap">
      <div class="mobile-navigation">
         <div class="mobile-logo">
            <a href="/"> <img src="/assets/images/navigations/logo.svg" alt=""> </a>
         </div>
         <div class="mobile-text">
            <span>PORT CITY COLOMBO <br>
               STUDENT DESIGN COMPETITION</span>
         </div>
         <div class="hamberger">
            <img class="ham-ico" src="/assets/images/navigations/hamberger.svg" alt="">
         </div>
      </div>
   </div>

   <section id="competioin-wrapper" class="middel-sec bg-section">
      <div class="inner-container">
         <ul class="transition">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
         </ul>
         <div class="tab-wrapper">
            <div class="tab-nav">
               <a href="/competition-one" class="com-btn com-one bg-color-set">
                  <div>
                     <span>COMPETITION ONE</span>
                     <h6>BEACH SCULPTURE – DREAM
                     </h6>
                  </div>
               </a>
               <a href="/competition-two" class="com-btn com-two">
                  <div>
                     <span>COMPETITION TWO</span>
                     <h6>INTERACTIVE WALL – VITALITY</h6>
                  </div>
               </a>

            </div>
         </div>
      </div>
      <main>
         <div class="comp-banner">
            <div class="inner-container">
               <div class="wrap-head">
                  <div class="short-nav">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                           <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                           <a href="#">BEACH SCULPTURE – DREAM</a>
                        </li>
                     </ol>
                  </div>
                  <h3 class="text-uppercase">BEACH SCULPTURE –
                     <a href="#" class="dream-font">
                        DREAM
                     </a>
                  </h3>
               </div>

               <div class="competion-banner-left">
                  <svg width="1304px" height="820px" viewbox="0 0 1304 820" version="1.1"
                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                     <defs>
                        <lineargradient x1="0%" y1="69.7776327%" x2="83.1728981%" y2="23.0386539%"
                           id="linearGradient-1">
                           <stop stop-color="#FFFB87" offset="0%"></stop>
                           <stop stop-color="#FF72D4" offset="100%"></stop>
                        </lineargradient>
                        <lineargradient x1="4.22646605%" y1="69.9317573%" x2="95.6047454%" y2="30.0349162%"
                           id="linearGradient-2">
                           <stop stop-color="#EAEC50" offset="0%"></stop>
                           <stop stop-color="#DD65BA" offset="48.3873522%"></stop>
                           <stop stop-color="#6654DE" offset="100%"></stop>
                        </lineargradient>
                        <lineargradient x1="95.711263%" y1="28.1379186%" x2="0%" y2="69.026511%" id="linearGradient-3">
                           <stop stop-color="#76EAF5" offset="0%"></stop>
                           <stop stop-color="#FE7DD8" offset="52.3147171%"></stop>
                           <stop stop-color="#FBFC8E" offset="100%"></stop>
                        </lineargradient>
                        <lineargradient x1="35.2175151%" y1="0%" x2="81.1075116%" y2="92.5884434%"
                           id="linearGradient-4">
                           <stop stop-color="#53EEFF" offset="0%"></stop>
                           <stop stop-color="#FA6AFF" offset="100%"></stop>
                        </lineargradient>
                        <lineargradient x1="42.8084326%" y1="0%" x2="65.1335697%" y2="92.5884434%"
                           id="linearGradient-5">
                           <stop stop-color="#34B3D2" offset="0%"></stop>
                           <stop stop-color="#AC18B3" offset="100%"></stop>
                        </lineargradient>
                        <lineargradient x1="95.0135031%" y1="43.7623838%" x2="4.74247685%" y2="56.1027282%"
                           id="linearGradient-6">
                           <stop stop-color="#FDFFA9" offset="0%"></stop>
                           <stop stop-color="#7EFFFE" offset="100%"></stop>
                        </lineargradient>
                        <lineargradient x1="96.8394482%" y1="42.4932138%" x2="9.50920791%" y2="56.2111428%"
                           id="linearGradient-7">
                           <stop stop-color="#D9FB59" offset="0%"></stop>
                           <stop stop-color="#68BBFF" offset="100%"></stop>
                        </lineargradient>
                     </defs>
                     <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="the-competition-1.0" transform="translate(-80.000000, -110.000000)">
                           <g id="Group" transform="translate(80.000000, 110.000000)">
                              <path
                                 d="M215,406.037736 L307.766667,311 L336,530 L215,406.037736 Z M356,551 L352,265.142857 L384,232 L456,306.571429 L384,580 L356,551 Z M425,538 L494,344 L563,410.042553 L425,538 Z"
                                 id="Combined-Shape-Copy" fill="#FFFFFF"></path>
                              <polygon id="Path-6" fill="url(#linearGradient-1)" opacity="0.6"
                                 points="336 530 215 406 1 530 0 730"></polygon>
                              <polygon id="Path-8" fill="url(#linearGradient-2)" opacity="0.55"
                                 points="215 406 308 311 0 487 0 530"></polygon>
                              <polygon id="Path-9" fill="url(#linearGradient-3)" opacity="0.5"
                                 points="356 551 384 580 0 820 0 770"></polygon>
                              <polygon id="Path-11" fill="url(#linearGradient-4)" opacity="0.5"
                                 points="352 265 160 0 275 0 384 232"></polygon>
                              <polygon id="Path-12" fill="url(#linearGradient-5)" opacity="0.5"
                                 points="384 232 456 307 316 0 275 0"></polygon>
                              <polygon id="Path-13" fill="url(#linearGradient-6)" opacity="0.5"
                                 points="494 344 1235 91 1304 149 563 411"></polygon>
                              <polygon id="Path-14" fill="url(#linearGradient-7)" opacity="0.5"
                                 points="425 538 563 411 1304 149 1255 248"></polygon>
                           </g>
                        </g>
                     </g>
                  </svg>
               </div>

               <div class="competition-banner-content">
                  <blockquote>
                     “The future belongs to those who believe in the beauty of their dreams” -Eleanor Roosevelt –
                  </blockquote>
                  <p class="regular-p">The beach is one of the five key elements that complete the vision of Port City
                     Colombo. The soft golden beach strip named as Marina is strategically introduced to the topography
                     of PCC, to create that balance in modern-day-living, which allows everyone to peel away the stress
                     and enjoy a relaxing family time gazing at the setting sun.</p>
                  <p class="regular-p">Theming the beach competition as “Dream”, it is a way of symbolising the very
                     nature of the Marina as it is a true embodiment of a “dream-like” sensation in Port City Colombo.
                     It gives a sense of freedom, belonging, and space where you can truly be yourself in an advancing
                     world. This is what you need to capture in your beach sculpture. Let your imagination run free and
                     attune yourself with the true purpose of the location and conceptualise your art to bring out your
                     version of the Marina dream in a modern world.</p>
               </div>

               <div class="competion-timeline-banner">
                  <div class="competion-timeline">
                     <h2 class="timeline-font">TIMELINE</h2>
                     <div class="timeline-row">
                        <span style="padding-left: 8px;">1</span>
                        <div class="content-row">
                           <p class="time-head">
                              Launch of the competition - 29th July 2020
                           </p>

                        </div>
                     </div>
                     <div class="timeline-row">
                        <span>2</span>
                        <div class="content-row">
                           <p class="time-head">
                              Closing Submissions - 25th August 2020</p>

                        </div>
                     </div>
                     <div class="timeline-row">
                        <span>3</span>
                        <div class="content-row">
                           <p class="time-head">Round 1 & 2 Jury evaluations - 26th August to 8th September</p>

                        </div>
                     </div>
                     <div class="timeline-row">
                        <span>4</span>
                        <div class="content-row">
                           <p class="time-head">Selection of Top 3 submissions - 9th September 2020</p>

                        </div>
                     </div>
                     <div class="timeline-row">
                        <span>5</span>
                        <div class="content-row">
                           <p class="time-head">Mentoring sessions - 10th September to 18th October 2020</p>

                        </div>
                     </div>
                     <div class="timeline-row">
                        <span>6</span>
                        <div class="content-row">
                           <p class="time-head">Announcing the winner - 4th Week of October 2020</p>

                        </div>
                     </div>
                  </div>
                  <div class="competion-timeline-image">
                     <div class="competion-one-slide">
                        <div class="swiper-wrapper">
                           <div class="swiper-slide">
                              <img src="/assets/images/competition/beach-sculpture-1.jpg" alt="">
                           </div>
                           <div class="swiper-slide">
                              <img src="/assets/images/competition/beach-sculpture-2.jpg" alt="">
                           </div>
                           <div class="swiper-slide">
                              <img src="/assets/images/competition/beach-sculpture-3.jpg" alt="">
                           </div>
                           <div class="swiper-slide">
                              <img src="/assets/images/competition/beach-sculpture-4.jpg" alt="">
                           </div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>

                     </div>
                     <div class="download-images-pack2">
                        <p>
                           <a href="/assets/zip/beach.zip" download="download">Click HERE
                           </a>
                           to download the image pack
                        </p>
                     </div>
                  </div>
               </div>

            </div>

         </div>
         <div class="download-images-pack">
            <p>
               <a href="/assets/zip/beach.zip" download="download">Click HERE
               </a>
               to download the image pack
            </p>
         </div>


      </main>

   </section>
   <section>
      <div class="middel-sec">
         <div class="inner-container">
            <div class="eligibale">
               <div class="inner-container">
                  <h2 class="timeline-font">ELIGIBILITY CRITERIA</h2>
                  <div class="el-wrap">
                     <div class="el-left">
                        <p class="regular-p">All the participants must be Sri Lankan undergraduate/graduate student
                           currently enrolled in any registered University in Sri Lanka or studying overseas, who are
                           reading in the fields of Art, Architecture, Design, Engineering and/or any other programme
                           that aligns with creativity.</p>

                        <p class="regular-p">Former students who have graduated within the last year, are welcome to
                           participate in the competition if the above criteria are met.</p>
                        <p class="regular-p">The participation can be done as a solo artist or as a team maximum of 03
                           students, while all the team members need to be within the eligibility criteria stated
                           above.Summary</p>
                     </div>
                     <div class="el-right">
                        <ul class="eligible-ul">
                           <li>All applicants must be Sri Lankan nationals.</li>
                           <li>All applicants should be undergraduate students currently enrolled in any registered
                              university in Sri Lanka or overseas, who are reading in the fields of Art, Architecture,
                              Design, Engineering and/or any other programme that aligns with creativity.</li>
                           <li>Graduates who have completed their degree in the academic year 2019/2020 are also
                              eligible to apply, provided that criteria 1 & 2 above are met. Proof of completion of the
                              degree has to be submitted.</li>
                           <li>In order to validate your application, the national identity card number is a must.</li>
                           <li>An applicant can apply as a solo applicant and as a member of a team. (A maximum of 2
                              submissions). An applicant cannot make multiple registrations by using the same NIC
                              number. (ex. If an applicant has already made a registration as a solo contestant to
                              either of competitions, and still willing to submit a group entry to the other
                              competition, same NIC holder should avoid registering as the principal contestant)</li>
                           <li>If an applicant is participating as a team, the team is limited to a maximum of 3 members
                           </li>
                           <li>The submission file(s) should be less than 20mb, in PDF format.</li>

                        </ul>
                     </div>
                  </div>
               </div>
            </div>

            <div class="rules">
               <div class="inner-container">
                  <h2 class="timeline-font">RULES & GUIDELINES</h2>

                  <div class="rules-sec">
                     <ul class="eligible-ul">
                        <li>The design project submitted to the project should be a unique concept</li>
                        <li>An individual can participate as a solo competitor and as one of the team limited to 03
                           entrants per team.</li>
                        <li>A competitor can submit, 1 concept per competition irrespective of competing as a solo
                           artist or as a team.</li>
                        <li>The submission must be in 2 A3 size pages, which needs to include a visual of the design in
                           2 angles, materials proposed for the design, brief on the concept.</li>
                        <li>The submission file(s) should be less than 20mb, in PDF format.</li>
                        <li>Please do not add any personal details on the submission materials (A3 PDFs), as we wish to
                           advance the competition impartially.</li>
                        <li>The winning concept will be constructed at the Marina beach in Port City Colombo premises.
                        </li>
                        <li>The lucky winner will cooperate with an international company in constructing the sculpture
                        </li>
                        <li>The maximum dimensions for the sculpture are 4 meters (length) x 4 meters (width) x 4 meters
                           (height)
                        </li>

                        <div class="download-sec">
                           <a href="/assets/pdf/RulesGuidelines-English.pdf" download> <button
                                 class="btn btn-primary main-btn">RULES & GUIDELINES <br> in English </button> </a>
                           <a href="/assets/pdf/RulesGuidelines-Sinhala.pdf" download> <button
                                 class="btn btn-primary main-btn">RULES & GUIDELINES <br> in Sinhale </button> </a>
                           <a href="/assets/pdf/RulesGuidelines-Tamil.pdf" download> <button
                                 class="btn btn-primary main-btn">RULES & GUIDELINES <br> in Tamil </button> </a>
                        </div>
                     </ul>

                  </div>
               </div>
            </div>

            <div class="rules">
               <div class="inner-container">
                  <h2 class="timeline-font">Competition Outline</h2>

                  <div class="rules-sec">
                     <ul class="eligible-ul">
                        <li>All applicants must register through the website to participate in the competition.</li>
                        <li>All submissions must be submitted via this website's online submission portal</li>
                        <li>The system will sort qualified submissions based on Jury’s markings.</li>
                        <li>Twenty (20) submissions will be shortlisted for each competition.</li>
                        <li>Out of the shortlisted 20 submissions, three (03) submissions will be shortlisted for Top 03
                           Awards and prizes.</li>
                        <li>The selected final round submissions will mandatorily go through an expert panel for its
                           feasibility study</li>
                        <li>A special design consultant will conduct a 02 weeks’ special workshop with the 3 finalist to
                           further develop selected proposals.</li>
                        <li>Honorary mentions will be decided and selected by a panel of experts.</li>
                     </ul>

                  </div>
               </div>
            </div>

            <div class="registration">
               <div class="inner-container">
                  <div class="header-fnt">
                     <h2 class="timeline-font">REGISTRATION PROCESS</h2>
                     <h2 class="step-fnt">Just 4 easy steps</h2>
                     <img class="right-rainbow-path" src="/assets/images/competition/rule-path.png" alt="">
                     <div class="progress-rule">
                        <img class="path" src="/assets/images/competition/path-dot.png" alt="">
                        <div class="path-box-wrap">
                           <div class="box1">
                              <img class="step-box" src="/assets/images/competition/step01.png" alt="">
                              <div class="wrap-cont">
                                 <span>Read the Rules and Guidelines</span>
                                 <br>
                                 <span>make sure you are eligible!</span>
                              </div>
                           </div>
                           <div class="box1">
                              <img class="step-box" src="/assets/images/competition/step02.png" alt="">
                              <div class="wrap-cont">
                                 <span>Create a Login for You/ Your Team Via Submission Portal</span>
                              </div>
                           </div>
                           <div class="box1">
                              <img class="step-box" src="/assets/images/competition/step03.png" alt="">
                              <div class="wrap-cont">
                                 <span>Upload Your Design & Proposal
                                 </span>
                                 <br>
                                 <span>use the index number!</span>
                              </div>
                           </div>
                           <div class="box1">
                              <img class="step-box" src="/assets/images/competition/step04.png" alt="">
                              <div class="wrap-cont">
                                 <span>Find us on Social Media</span>
                                 <br>
                                 <span>Like us and share the news!</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="competion-submition">

               <div class="submition-section">
                  <h2 class="timeline-font">SUBMISSIONS</h2>
                  <p>Ready to take the challenge and prove your strength to the world? Let’s make it happen</p>

                  <button class="btn btn-primary second-btn">TAKE ME TO SUBMIT PORTAL</button>
               </div>
               <div class="need-help-section">
                  <h2 class="timeline-font">NEED HELP?</h2>
                  <p>Need to know more about the competition before you head on?
                  </p>

                  <a href="/faq." class="btn btn-primary btn-primary second-btn second-btn second-btn">GO TO FAQ</a>
               </div>

            </div>

         </div>


         <div id="home-jury" class="competition-home">
            <div class="jury-home">
               <h2 class="text-uppercase text-center port-text">THE JURY</h2>
               <div class="jury-images-wrap">
                  <div class="jury-item box1-slide">
                     <img class="jury-img-bnner" src="/assets/images/jury/newimage01.jpg" alt="">
                     <div class="jury-content">
                        <span class="text-uppercase posi">Senior Textile Designer & Lecture</span>
                        <h5>Shilanthi Abayagunawardana</h5>

                     </div>
                     <div class="info">
                        <p class="regular-p">“As you take on this “New World” of opportunity ,be responsible in your
                           freedom , let wisdom of the past navigate you towards clarity, be brave in your creation
                           ,infuse exceptional value and quality which breathes, timelessness !”</p>
                     </div>
                  </div>
                  <div class="jury-item box2-slide">
                     <img src="/assets/images/jury/newimage02.jpg" alt="">
                     <div class="jury-content">
                        <span class="text-uppercase posi">Architect</span>
                        <h5>Ruchi Jayanathan</h5>

                     </div>
                     <div class="info">
                        <p class="regular-p">Sculpture to me - is the art of playing with solid and void, navigating the
                           nature and soul of the place and materials in use … to provoke a dialogue, an emotion or a
                           spiritual response</p>
                     </div>
                  </div>
                  <div class="jury-item box3-slide">
                     <img src="/assets/images/jury/newimage03.jpg" alt="">
                     <div class="jury-content">
                        <span class="text-uppercase posi">Contemporary Artist & Designer</span>
                        <h5>Anoma Wijewardene</h5>
                     </div>
                     <div class="info">
                        <p class="regular-p">In this cataclysmic and historic moment when public art and statues which
                           are questionable are being torn down it is vital to approach this venture with a deep dive
                           into concept and meaning. Considering the zeitgeist of the moment and also for all time,
                           sharing the universal call for justice and equality, and bringing visual peace and harmony
                           might be a way to approach this opportunity.</p>
                     </div>
                  </div>
                  <div class="jury-item box4-slide">
                     <img src="/assets/images/jury/newimage04.jpg" alt="">
                     <div class="jury-content">
                        <span class="text-uppercase posi">Associate Director - Atkins</span>
                        <h5>Xia Yuan</h5>
                     </div>
                     <div class="info">
                        <p class="regular-p">As a landscape designer, he often uses appropriate public art to create the
                           highlights of the city. Good public art is the spiritual wealth of a city, which can resonate
                           with people in culture, entertainment and sometimes emotion. When evaluating the success or
                           failure of a public art work, apart from its aesthetic value, we should also consider the
                           social value it brings, for example a sense of belonging, a sense of pride of the city, the
                           embodiment of regional cultural characteristics, the transmission of positive values, and
                           even the solution of social problems.</p>
                     </div>
                  </div>


               </div>
            </div>
         </div>






         <div class="inner-container">
            <div class="awards">
               <h2 class="text-uppercase text-center port-text">AWARDS</h2>

               <div class="wrap-winner-sec">
                  <div class="the-winner">
                     <div class="wrap-suv-title2">
                        <h5 class="winner-sub-title">The Winner</h5>
                     </div>

                     <div class="wrap-winner-img">
                        <img src="/assets/images/competition/third03.jpg" alt="">
                        <span>
                           Tharindu Perera, Pulasthi Handunge &amp; Viranga Waduge
                           <br>City School of Architecture</span>
                           <span style="padding-top: 0px">Cash Price of 300,000 LKR</span>
                     </div>
                  </div>
                  <div class="top-three-sec">
                     <div class="wrap-suv-title">
                        <h5 class="winner-sub-title">Top Three</h5>
                        <p class="text-center">From round one</p>
                     </div>

                     <div class="top-three">
                        <div class="wrap-winner-img">
                           <img src="/assets/images/competition/third01.jpeg" alt="">
                           <span class="runnerup-txt">Runner Up</span>
                           <span>
                              Rochelle Fernando
                              <br>Academy of Design</span>
                        </div>
                        <div class="wrap-winner-img">
                           <img src="/assets/images/competition/third02.jpeg" alt="">
                           <span class="runnerup-txt">Runner Up</span>
                           <span>
                              Lakal Piyarathna
                              <br>University of Moratuwa</span>
                        </div>
                        <div class="wrap-winner-img">
                           <img src="/assets/images/competition/third03.jpg" alt="">
                           <span class="invisible runnerup-txt">3st Runner Up</span>
                           <span>
                              Tharindu Perera, Pulasthi Handunge & Viranga Waduge
                              <br>City School of Architecture</span>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>


      </div>
   </section>

   <div class="middel-sec">
      <footer>
         <div class="wrap-footer">
            <div class="port-logo">
               <img src="/assets/images/footer/port-footer.png" alt="">
            </div>
            <div class="ft-dis">
               <p>©2020. PCC Student Design Competition 2020 </p>
               <p class="solution">Solution By <a href="https://www.saberion.com/" target="_blank">SABERION</a></p>
               <div class="social">

               </div>
            </div>

            <div class="footer-social-link">
               <a class="lgz3" href="#">
                  <img src="/assets/images/navigations/facebook.svg" alt="">
               </a>
               <a class="lgz3" href="#">
                  <img src="/assets/images/navigations/icon.svg" alt="">
               </a>
               <a class="lgz3" href="#">
                  <img src="/assets/images/navigations/insta.svg" alt="">
               </a>
               <a class="lgz3" href="#">
                  <img src="/assets/images/navigations/linkedin.svg" alt="">
               </a>
               <a class="lgz3" href="#">
                  <img src="/assets/images/navigations/twitter.svg" alt="">
               </a>
            </div>
         </div>
      </footer>
   </div>
   <div class="right-menu">
      <div class="hamberger">
         <img class="ham-ico" src="/assets/images/navigations/hamberger.svg" alt="">
      </div>

      <div class="slider-bulet">
         <div class="swiper-pagination"></div>
      </div>

      <div class="chat-section">

         <div class="text-ani-inner port-right-logo">
            <a href="#">
               <img src="/assets/images/navigations/port-logo.svg" alt="">
            </a>

         </div>

         <!--Start of Tawk.to Script-->
         <script type="text/javascript">
            var Tawk_API = Tawk_API || {},
               Tawk_LoadStart = new Date();
            (function () {
               var s1 = document.createElement("script"),
                  s0 = document.getElementsByTagName("script")[0];
               s1.async = true;
               s1.src = 'https://embed.tawk.to/5f227d964f3c7f1c910d7fdc/default';
               s1.charset = 'UTF-8';
               s1.setAttribute('crossorigin', '*');
               s0
                  .parentNode
                  .insertBefore(s1, s0);
            })();
         </script>
         <!--End of Tawk.to Script-->
      </div>
   </div>
   <script>
      var siteBaseUrl = '';
   </script>
   <script src="/assets/js/lib.js"></script>
   <script src="/assets/js/main.js"></script>
</body>

</html>
