@if ($medias->isNotEmpty())
    <div id="insta" class="">
        <div class="wrap-insta">
            <div class="insta-adi">
                <img src="/assets/images/insta/instagram.svg" alt="">
                <h2>#PCCSDC2020</h2>
            </div>

            <div class="gallery">
                @foreach ($medias as $media)
                    <div class="gal">
                        <img src="{{ $media->getThumbnailSrc() }}">
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif