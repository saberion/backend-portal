<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Envision The Future</title>
    <link rel="stylesheet" href="/assets/css/lib.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="async" src="https://www.googletagmanager.com/gtag/js?id=UA-174141483-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-174141483-1');
    </script>

</head>

<body class="onload aod-faq">
    <div class="left-menu">
        <div class="logo">
            <a href="/">
                <img src="/assets/images/navigations/logo.svg" alt="">
            </a>
        </div>
        <div class="pcc-sec lg1">
            <span>PCC STUDENT DESIGN COMPETITION 2020</span>
        </div>
        <div class="social-media">
            <div class="text-ani-inner">
                <a class="lgz3" href="https://www.facebook.com/PortCityLK" target="_blank">
                    <img src="/assets/images/navigations/facebook.svg" alt="">
                </a>
            </div>

            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.youtube.com/channel/UCu-RLmtH6Tm1Y_AbmvFuFog/featured">
                    <img src="/assets/images/navigations/icon.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.instagram.com/portcitycolombo/">
                    <img src="/assets/images/navigations/insta.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.linkedin.com/company/port-city-colombo/">
                    <img src="/assets/images/navigations/linkedin.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://twitter.com/PortCityColombo">
                    <img src="/assets/images/navigations/twitter.svg" alt="">
                </a>
            </div>

        </div>
    </div>

    <div class="menu-wrapper">
        <div class="menu">
            <div class="data">
                <ul>
                    <li><a class="nav-data" href="/">HOME</a></li>
                    <li><a class="nav-data" href="/about">ABOUT PCC</a></li>
                    <li><a class="nav-data" href="/competition-one">THE COMPETITION</a></li>
                    <li><a class="nav-data" href="/media">MEDIA</a></li>
                    <li><a class="nav-data" href="/faq">FAQ</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div id="loader-wrapper">
        <div id="loader">
            <img src="/assets/images/navigations/logo.svg" alt="">
        </div>

        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>

    </div>






    <div class="mobile-navigation-wrap">
        <div class="mobile-navigation">
            <div class="mobile-logo">
                <a href="/"> <img src="/assets/images/navigations/logo.svg" alt=""> </a>
            </div>
            <div class="mobile-text">
                <span>PORT CITY COLOMBO <br>
                    STUDENT DESIGN COMPETITION</span>
            </div>
            <div class="hamberger">
                <img class="ham-ico" src="/assets/images/navigations/hamberger.svg" alt="">
            </div>
        </div>
    </div>

    <div class="middel-sec">
        <div class="inner-container" style="position: relative;">

            <section class="faq-wrapper faqbg">

                <div class="short-nav">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="#">Faq</a>
                        </li>
                    </ol>
                </div>
                <h3 class="main-banner-tittle-ani">FREQUENTLY ASKED QUESTIONS</h3>

                <div class="search">
                    <input type="email" placeholder="Search for answers">
                </div>

                <div class="quections">


                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>I graduated last year; can I still participate?</h5>
                        </div>
                        <p>Yes. We allow former students to participate if you have graduated within the past year. If
                            you have graduated 2 years ago, then you are not able to participate.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>Is there an age limit?</h5>
                        </div>
                        <p>All undergraduates, and students who have graduated within in the last year are able to
                            participate if eligibility criteria are met. Refer to the competition page for more details.
                        </p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>I&#39;m not Sri Lankan, Can I participate?</h5>
                        </div>
                        <p>The competition is only open for Sri Lankan nationalities who are studying in local or a
                            foreign university. But keep following our social accounts, there’ll be future opportunities
                            for you. </p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>Can I participate in both competitions?</h5>
                        </div>
                        <p>Yes, we encourage multiple submissions, but the participant will be allowed to submit one
                            design per competition irrespective if it’s a solo submission or a team. </p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>Can I visit the site? </h5>
                        </div>
                        <p>For the time being you are not allowed to visit the PCC premises unless you are one of the
                            finalists. But please keep in touch via our social media for the latest updates on the
                            competition.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>Can anybody come to the unveiling ceremony?</h5>
                        </div>
                        <p>For the time being we have restrictions in relations to the current conditions, however there
                            is a possibility for the situation to be improved for the better. Therefore, follow us
                            through our social media, for the latest updates.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>Can a high school student participate? </h5>
                        </div>
                        <p>We appreciate your enthusiasm and understand your willingness to participate in the
                            competition; however, this event is planned only for university students. But keep up to
                            date by following our social accounts for future competitions! </p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>My design didn&#39;t win any prize, can it still be built?</h5>
                        </div>
                        <p>For now, the winning concept will be built at the Port City Colombo. If we receive many
                            high-quality entries, we can consider your proposal in the future </p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>Do I have the copyright of the design?</h5>
                        </div>
                        <p>The winning projects will be copyrighted to Port City Colombo, while credits are given to the
                            creator/designer. The copyrights for the rest of the submissions can be claimed by the
                            respective designer</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>I want to join the competition with my friends. Is there a limit on the number of
                                members in a team?</h5>
                        </div>
                        <p>Yes, the maximum number of members on a team is three and all members should be compliant
                            with the eligibility criteria.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>Can I participate on my own?</h5>
                        </div>
                        <p>Yes, you can participate as an individual, and submit your design concepts to both.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>Can I submit more than one project?</h5>
                        </div>
                        <p>Yes, the maximum number of entries is limited to one entry per competition, per entrant.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>Can I participate both as a sole entrant and with a group?</h5>
                        </div>
                        <p>Sure, if there won’t be any conflicts and everyone can work with an open mind, yes you may.
                            However, there’s a limitation to it, following is how you can participate in the
                            competitions. Competition A – Solo Artist, Competition B – Part of a Team Competition A –
                            Solo Artist, Competition B – Solo Artist Competition A – Part of a Team, Competition B –
                            Part of a team (this could be the same team or two different teams)</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>When will the winners be announced?</h5>
                        </div>
                        <p>Please refer the competition page for the timeline and updates.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>What is the deadline for the competition?</h5>
                        </div>
                        <p>Please refer the competition page for the timeline and updates.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>I would like to know if I can submit a 3D model of the sculpture.</h5>
                        </div>
                        <p>We won’t be accepting physical models, but you can create a 3D model of your concept and
                            submit photographs of the same as mentioned in the submission guideline.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>What are the requirements for the digital A3 submission?</h5>
                        </div>
                        <p>You must submit 1 PDF of the design concept with 2 visuals from different angles you prefer.
                            Apart from that, the 2nd PDF should be submitted separately for the materials required and
                            description of the project.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>What size should the entry images be?</h5>
                        </div>
                        <p>20 MB maximum per submission for all 02 files.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>What are the requirements that I must consider for my concept?</h5>
                        </div>
                        <p>You must consider the wind factor, the materials, longevity, safety and maintenance of the
                            sculpture and finally the relevance to the theme.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>Will you be sponsoring for the materials?</h5>
                        </div>
                        <p>The PCC will pay for the sculpture/interactive wall fabrication, while you will have the
                            opportunity to collaborate with the top professionals and learn from them</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>What are the entry requirements for a group?</h5>
                        </div>
                        <p>Each member must submit their name, the name of their institution, course title, the year
                            their studies commenced, email addresses and contact numbers. Note that a group can only
                            have 3 members maximum and should be within the eligibility criteria. Please refrain from
                            mentioning any personal details on the content submitted, as we would like to conduct the
                            competition fairly.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>If I pass the first round, when can I develop my sculpture for the judges?</h5>
                        </div>
                        <p>Once we receive all the entries, we will be selecting and announcing the 3 finalists from
                            each competition. They will be working with our team and receive professional guidance to
                            improve their concept and allow the competitors time to further groom their project. Then on
                            the deadline, we will again go through the improved concepts and select a winner from both
                            the competition, and the winning design will be constructed by PCC. The timelines for each
                            milestone will be updated on the respective competition pages and will be announced via
                            social media.</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>My registration link is not working, what do I do?</h5>
                        </div>
                        <p>You can drop an email to PCCSDCenquiry2020@chec.lk</p>
                    </div>

                    <div class="q-item">
                        <div class="q-tittle">
                            <h5>I’m unable to upload my designs, what can I do?</h5>
                        </div>
                        <p>Please drop an email to PCCSDCenquiry2020@chec.lk with screenshots of the error you are
                            seeing.</p>
                    </div>


                </div>
                <div class="wrpa-qa">
                    <div class="moreQA">
                        <p>Have more questions but not listed above?</p>
                        <p>Please send your queries to
                            <a href="mailto:PCCSDCenquiry2020@chec.lk">PCCSDCenquiry2020@chec.lk</a>, we'll make sure
                            that you'll get your answers soon.</p>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="middel-sec">
        <footer>
            <div class="wrap-footer">
                <div class="port-logo">
                    <img src="/assets/images/footer/port-footer.png" alt="">
                </div>
                <div class="ft-dis">
                    <p>©2020. PCC Student Design Competition 2020 </p>
                    <p class="solution">Solution By <a href="https://www.saberion.com/" target="_blank">SABERION</a></p>
                    <div class="social">

                    </div>
                </div>

                <div class="footer-social-link">
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/facebook.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/icon.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/insta.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/linkedin.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/twitter.svg" alt="">
                    </a>
                </div>
            </div>
        </footer>
    </div>
    <div class="right-menu">
        <div class="hamberger">
            <img class="ham-ico" src="/assets/images/navigations/hamberger.svg" alt="">
        </div>

        <div class="slider-bulet">
            <div class="swiper-pagination"></div>
        </div>

        <div class="chat-section">

            <div class="text-ani-inner port-right-logo">
                <a href="#">
                    <img src="/assets/images/navigations/port-logo.svg" alt="">
                </a>

            </div>

            <!--Start of Tawk.to Script-->
            <script type="text/javascript">
                var Tawk_API = Tawk_API || {},
                    Tawk_LoadStart = new Date();
                (function () {
                    var s1 = document.createElement("script"),
                        s0 = document.getElementsByTagName("script")[0];
                    s1.async = true;
                    s1.src = 'https://embed.tawk.to/5f227d964f3c7f1c910d7fdc/default';
                    s1.charset = 'UTF-8';
                    s1.setAttribute('crossorigin', '*');
                    s0
                        .parentNode
                        .insertBefore(s1, s0);
                })();
            </script>
            <!--End of Tawk.to Script-->
        </div>
    </div>
    <script>
        var siteBaseUrl = '';
    </script>
    <script src="/assets/js/lib.js"></script>
    <script src="/assets/js/main.js"></script>
</body>

</html>