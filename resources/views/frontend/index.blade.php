<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Envision The Future</title>
    <link rel="stylesheet" href="/assets/css/lib.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="async" src="https://www.googletagmanager.com/gtag/js?id=UA-174141483-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-174141483-1');
    </script>

</head>

<body class="onload aod-home">
    <div class="left-menu">
        <div class="logo">
            <a href="/">
                <img src="/assets/images/navigations/logo.svg" alt="">
            </a>
        </div>
        <div class="pcc-sec lg1">
            <span>PCC STUDENT DESIGN COMPETITION 2020</span>
        </div>
        <div class="social-media">
            <div class="text-ani-inner">
                <a class="lgz3" href="https://www.facebook.com/PortCityLK" target="_blank">
                    <img src="/assets/images/navigations/facebook.svg" alt="">
                </a>
            </div>

            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.youtube.com/channel/UCu-RLmtH6Tm1Y_AbmvFuFog/featured">
                    <img src="/assets/images/navigations/icon.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.instagram.com/portcitycolombo/">
                    <img src="/assets/images/navigations/insta.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.linkedin.com/company/port-city-colombo/">
                    <img src="/assets/images/navigations/linkedin.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://twitter.com/PortCityColombo">
                    <img src="/assets/images/navigations/twitter.svg" alt="">
                </a>
            </div>

        </div>
    </div>

    <div class="menu-wrapper">
        <div class="menu">
            <div class="data">
                <ul>
                    <li><a class="nav-data" href="/">HOME</a></li>
                    <li><a class="nav-data" href="/about">ABOUT PCC</a></li>
                    <li><a class="nav-data" href="/competition-one">THE COMPETITION</a></li>
                    <li><a class="nav-data" href="/media">MEDIA</a></li>
                    <li><a class="nav-data" href="/faq">FAQ</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div id="loader-wrapper">
        <div id="loader">
            <img src="/assets/images/navigations/logo.svg" alt="">
        </div>

        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>

    </div>



    <div class="mobile-navigation-wrap">
        <div class="mobile-navigation">
            <div class="mobile-logo">
                <a href="/"> <img src="/assets/images/navigations/logo.svg" alt=""> </a>
            </div>
            <div class="mobile-text">
                <span>PORT CITY COLOMBO <br>
                    STUDENT DESIGN COMPETITION</span>
            </div>
            <div class="hamberger">
                <img class="ham-ico" src="/assets/images/navigations/hamberger.svg" alt="">
            </div>
        </div>
    </div>

    <section class="middel-sec">
        <div class="wrap-over">
            <div class="">
                <div id="banner" class="main-banner banner-section">
                    <div class="scroll-indicator" id="section01" data-scroll-indicator-title=""></div>
                    <div class="banner-text-section">
                        <div class="text-ani-inner">
                            <h1 class="banner-text-ani">Envision The Future</h1>
                        </div>
                        <div class="text-ani-inner">
                            <h2 class="banner-text-ani">Port City Colombo
                                <br>Student Design Competition</h2>
                        </div>

                        <div class="competition-box">
                            <a href="/media#gallery-section" class="box box1">
                                <div class="upper-box">
                                    <span class="comp">COMPETITION</span>
                                    <span class="one">
                                        BEACH SCULPTURE</span>
                                </div>
                                {{-- <span class="enter-now">Top 10 
                                    <br>
                                    Submissions</span> --}}
                                    <span class="enter-now">Winning Designs</span>
                            </a>
                            <a href="/media#gallery-section" id="a" class="box box2">
                                <div class="upper-box">
                                    <span class="comp">COMPETITION</span>
                                    <span class="one">
                                        INTERACTIVE WALL</span>
                                </div>
                                <span class="enter-now">Winning Designs</span>
                                {{-- <span class="enter-now">Top 10
                                    <br>
                                    Submissions</span> --}}
                            </a>
                        </div>
                    </div>

                    <div class="mask-slider">
                        <div class="overlay">

                        </div>
                        <div class="bg-slider">
                            <div class="swiper-overlay">
                                <div class="swiper-wrapper">

                                    <div class="swiper-slide">
                                        <img src="/assets/images/body/portcity01.jpg" alt="">
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="/assets/images/body/portcity02.jpg" alt="">
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="/assets/images/body/portcity03.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="scroll-indicator" id="section02" data-scroll-indicator-title=""></div>
                <div id="home-competition" class="competitionMain competition-home">

                    <div class="wrap-comp-home">
                        <div class="competition-home-left">
                            <div class="comp-logo">
                                <img src="/assets/images/competition/comp-logo.svg" alt="">
                            </div>
                            <div class="com-left-discript">
                                <h4 class="reveal">PORT CITY COLOMBO</h4>
                                <h4 class="reveal text-uppercase">Student</h4>
                                <h4 class="reveal">DESIGN COMPETITION</h4>
                                <span>2020</span>
                            </div>
                        </div>

                        <div class="competition-home-discript">
                            <h2>THE COMPETITION</h2>

                            <p class="regular-p">
                                The PCC Student Design Competition; themed as “Envision the Future”, is a unique
                                opportunity for the Sri Lankan youth to bring out their creative talents under the
                                global spotlight, by envisioning the future of Sri Lanka. The aim of the competition is
                                to celebrate the importance of design in the planning and development of Port City
                                Colombo – and portraying it through the eyes of our gifted future generation.
                            </p>

                            <a href="/competition-one">
                                <button class="btn btn-primary main-btn text-uppercase">read more</button>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="scroll-indicator" id="section03" data-scroll-indicator-title=""></div>
                <div class="wrap-detals-section">

                    <div id="requeir-slide" class="DtailsPage">
                        <div class="wrap-requeir">
                            <div class="mid-nav">

                                <div class="menu-slide"></div>
                            </div>
                        </div>

                    </div>
                    <div class="req-slide">

                        <div class="left-panel"></div>
                        <div class="right-panel"></div>
                        <div class="swiper-wrapper">

                            <div class="swiper-slide" data-hash="slide1">
                                <h2>Eligibility</h2>
                                <p class="regular-p">We welcome any Sri Lankan undergraduate/graduate student currently
                                    enrolled in any registered University in Sri Lanka or studying overseas, who are
                                    reading in the fields of Art, Architecture, Design, Engineering and/or any other
                                    programme that aligns with creativity.Former students who have graduated within the
                                    last year, are welcome to participate in the competition if the above criteria are
                                    met.The participation can be done as a solo artist or as a team maximum of 03
                                    students, while all the team members need to be within the eligibility criteria
                                    stated above.</p>
                            </div>
                            <div class="swiper-slide" data-hash="slide2">
                                <h2>Awards</h2>
                                <p class="regular-p">Apart from the global recognition, opportunities for many future
                                    commissions and having your design project built in the Port City Colombo, the
                                    winners will be awarded cash prizes and official acknowledgement of their talents by
                                    an esteemed panel.After the first round of selections, the 3 finalists will receive
                                    an award of LKR 50,000 and proceed towards the final phase, where one lucky finalist
                                    will emerge as the victor receiving a winning prize of LKR 250,000/-. The runners up
                                    will be awarded trophies of recognition, and honourable mentions will be awarded to
                                    a selection of gifted artistic souls.</p>
                            </div>
                            <div class="swiper-slide" data-hash="slide13">
                                <h2>Requirements</h2>
                                <p class="regular-p">We will be conducting two competitions featuring your talent in an
                                    interactive wall art competition and in a Beach Sculpture competition. You can
                                    participate in both as a solo artist or as a team limited to a maximum of 03
                                    contenders.To participate, you are required to register and submit your design
                                    proposal to proceed to phase two. The submissions should include a visual of your
                                    design from 2 angles, description of the materials used, and the plan of execution.
                                    For a comprehensive guideline refer to the competition page.</p>
                            </div>
                            <div class="swiper-slide" data-hash="slide1">
                                <h2>Timeline</h2>
                                <ul>
                                    <li>
                                        <p class="regular-p">01: The official commencement of the competition - 29 july
                                            2020
                                        </p>
                                    </li>
                                    <li>
                                        <p class="regular-p">02: Acceptance of application with project proposals from
                                            29th July through 25th August for Beach Sculpture and 8th September 2020 for
                                            Interactive Wall</p>
                                    </li>
                                    <li>
                                        <p class="regular-p">03: Announcing of the top 03 submissions and progressing
                                            the competition to stage 02 – conversion of the concept.</p>
                                    </li>
                                    <li>
                                        <p class="regular-p">04: The announcement of the winner of the competition.</p>
                                    </li>
                                    <li>
                                        <p class="regular-p">05: Completion of constructing the artwork.</p>
                                    </li>
                                    <li>
                                        <p class="regular-p">06: Grand finale and the unveiling ceremony.</p>
                                    </li>
                                </ul>

                            </div>

                        </div>
                    </div>

                    <div class="nav-req-sec">
                        <img class="req-left" src="/assets/images/body/req-left.png" alt="">
                        <img class="req-right" src="/assets/images/body/req-right.png" alt="">
                    </div>
                </div>


                <div class="scroll-indicator" id="section04" data-scroll-indicator-title=""></div>
                <div id="home-jury" class="competition-home">
                    <div class="jury-home">
                        <h2 class="text-uppercase text-center port-text">THE JURY</h2>
                        <div class="jury-images-wrap">
                            <div class="jury-item box1-slide">
                                <img class="jury-img-bnner" src="/assets/images/jury/newimage01.jpg" alt="">
                                <div class="jury-content">
                                    <span class="text-uppercase posi">Senior Textile Designer & Lecture</span>
                                    <h5>Shilanthi Abayagunawardana</h5>

                                </div>
                                <div class="info">
                                    <p class="regular-p">“As you take on this “New World” of opportunity ,be responsible
                                        in your freedom , let wisdom of the past navigate you towards clarity, be brave
                                        in your creation ,infuse exceptional value and quality which breathes,
                                        timelessness !”</p>
                                </div>
                            </div>
                            <div class="jury-item box2-slide">
                                <img src="/assets/images/jury/newimage02.jpg" alt="">
                                <div class="jury-content">
                                    <span class="text-uppercase posi">Architect</span>
                                    <h5>Ruchi Jayanathan</h5>

                                </div>
                                <div class="info">
                                    <p class="regular-p">Sculpture to me - is the art of playing with solid and void,
                                        navigating the nature and soul of the place and materials in use … to provoke a
                                        dialogue, an emotion or a spiritual response</p>
                                </div>
                            </div>
                            <div class="jury-item box3-slide">
                                <img src="/assets/images/jury/newimage03.jpg" alt="">
                                <div class="jury-content">
                                    <span class="text-uppercase posi">Contemporary Artist & Designer</span>
                                    <h5>Anoma Wijewardene</h5>
                                </div>
                                <div class="info">
                                    <p class="regular-p">In this cataclysmic and historic moment when public art and
                                        statues which are questionable are being torn down it is vital to approach this
                                        venture with a deep dive into concept and meaning. Considering the zeitgeist of
                                        the moment and also for all time, sharing the universal call for justice and
                                        equality, and bringing visual peace and harmony might be a way to approach this
                                        opportunity.</p>
                                </div>
                            </div>
                            <div class="jury-item box4-slide">
                                <img src="/assets/images/jury/newimage04.jpg" alt="">
                                <div class="jury-content">
                                    <span class="text-uppercase posi">Associate Director - Atkins</span>
                                    <h5>Xia Yuan</h5>
                                </div>
                                <div class="info">
                                    <p class="regular-p">As a landscape designer, he often uses appropriate public art
                                        to create the highlights of the city. Good public art is the spiritual wealth of
                                        a city, which can resonate with people in culture, entertainment and sometimes
                                        emotion. When evaluating the success or failure of a public art work, apart from
                                        its aesthetic value, we should also consider the social value it brings, for
                                        example a sense of belonging, a sense of pride of the city, the embodiment of
                                        regional cultural characteristics, the transmission of positive values, and even
                                        the solution of social problems.</p>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>





                <div id="owl-jury" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="jury-item-mobile box1-slide">
                            <img class="jury-img-bnner" src="/assets/images/jury/newimagemob01.jpg" alt="">
                            <div class="jury-content">
                                <span class="text-uppercase posi">Senior Textile Designer & Lecture</span>
                                <h5>Shilanthi Abayagunawardana</h5>

                            </div>
                            <div class="info">
                                <p class="regular-p">“As you take on this “New World” of opportunity ,be responsible in
                                    your freedom , let wisdom of the past navigate you towards clarity, be brave in your
                                    creation ,infuse exceptional value and quality which breathes, timelessness !”</p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="jury-item-mobile box2-slide">
                            <img src="/assets/images/jury/newimagemob02.jpg" alt="">
                            <div class="jury-content">
                                <span class="text-uppercase posi">Architect</span>
                                <h5>Ruchi Jayanathan</h5>

                            </div>
                            <div class="info">
                                <p class="regular-p">Sculpture to me - is the art of playing with solid and void,
                                    navigating the nature and soul of the place and materials in use … to provoke a
                                    dialogue, an emotion or a spiritual response</p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="jury-item-mobile box3-slide">
                            <img src="/assets/images/jury/newimagemob03.jpg" alt="">
                            <div class="jury-content">
                                <span class="text-uppercase posi">Contemporary Artist & Designer</span>
                                <h5>Anoma Wijewardene</h5>
                            </div>
                            <div class="info">
                                <p class="regular-p">In this cataclysmic and historic moment when public art and statues
                                    which are questionable are being torn down it is vital to approach this venture with
                                    a deep dive into concept and meaning. Considering the zeitgeist of the moment and
                                    also for all time, sharing the universal call for justice and equality, and bringing
                                    visual peace and harmony might be a way to approach this opportunity.</p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="jury-item-mobile box4-slide">
                            <img src="/assets/images/jury/newimagemob04.jpg" alt="">
                            <div class="jury-content">
                                <span class="text-uppercase posi">PROFESSOR</span>
                                <h5>Eva Williams</h5>
                            </div>
                            <div class="info">
                                <p class="regular-p">As a landscape designer, he often uses appropriate public art to
                                    create the highlights of the city. Good public art is the spiritual wealth of a
                                    city, which can resonate with people in culture, entertainment and sometimes
                                    emotion. When evaluating the success or failure of a public art work, apart from its
                                    aesthetic value, we should also consider the social value it brings, for example a
                                    sense of belonging, a sense of pride of the city, the embodiment of regional
                                    cultural characteristics, the transmission of positive values, and even the solution
                                    of social problems.</p>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="scroll-indicator" id="section05" data-scroll-indicator-title=""></div>


                <div class="competion-submition">

                    <div class="submition-section">
                        <h2 class="timeline-font">ENTER NOW!</h2>
                        <p>Have you got what it takes to voice your opinions competing with the visionaries in the
                            industry? Here’s your chance to test your creative drive. Register Now</p>

                        <a href="/login" class="btn btn-primary second-btn">TAKE ME TO SUBMIT PORTAL</a>
                    </div>
                    <div class="need-help-section">
                        <h2 class="timeline-font">LOGIN</h2>
                        <p>Already registered? And want to submit your work?</p>

                        <a href="/login" class="btn btn-primary second-btn">LOGIN</a>
                    </div>

                </div>

                <div class="scroll-indicator" id="section06" data-scroll-indicator-title=""></div>

                @include('frontend.instagram', ['medias' => $medias])
            </div>
        </div>
    </section>

    <div class="middel-sec">
        <footer>
            <div class="wrap-footer">
                <div class="port-logo">
                    <img src="/assets/images/footer/port-footer.png" alt="">
                </div>
                <div class="ft-dis">
                    <p>©2020. PCC Student Design Competition 2020 </p>
                    <p class="solution">Solution By <a href="https://www.saberion.com/" target="_blank">SABERION</a></p>
                    <div class="social">

                    </div>
                </div>

                <div class="footer-social-link">
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/facebook.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/icon.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/insta.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/linkedin.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/twitter.svg" alt="">
                    </a>
                </div>
            </div>
        </footer>
    </div>
    <div class="right-menu">
        <div class="hamberger">
            <img class="ham-ico" src="/assets/images/navigations/hamberger.svg" alt="">
        </div>

        <div class="slider-bulet">
            <div class="swiper-pagination"></div>
        </div>

        <div class="chat-section">

            <div class="text-ani-inner port-right-logo">
                <a href="#">
                    <img src="/assets/images/navigations/port-logo.svg" alt="">
                </a>

            </div>

            <!--Start of Tawk.to Script-->
            <script type="text/javascript">
                var Tawk_API = Tawk_API || {},
                    Tawk_LoadStart = new Date();
                (function () {
                    var s1 = document.createElement("script"),
                        s0 = document.getElementsByTagName("script")[0];
                    s1.async = true;
                    s1.src = 'https://embed.tawk.to/5f227d964f3c7f1c910d7fdc/default';
                    s1.charset = 'UTF-8';
                    s1.setAttribute('crossorigin', '*');
                    s0
                        .parentNode
                        .insertBefore(s1, s0);
                })();
            </script>
            <!--End of Tawk.to Script-->
        </div>
    </div>
    <script>
        var siteBaseUrl = '';
    </script>
    <script src="/assets/js/lib.js"></script>
    <script src="/assets/js/main.js"></script>
</body>

</html>
