<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Envision The Future</title>
    <link rel="stylesheet" href="/assets/css/lib.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="async" src="https://www.googletagmanager.com/gtag/js?id=UA-174141483-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-174141483-1');
    </script>

</head>

<body class="onload aod-about">
    <div class="left-menu">
        <div class="logo">
            <a href="/">
                <img src="/assets/images/navigations/logo.svg" alt="">
            </a>
        </div>
        <div class="pcc-sec lg1">
            <span>PCC STUDENT DESIGN COMPETITION 2020</span>
        </div>
        <div class="social-media">
            <div class="text-ani-inner">
                <a class="lgz3" href="https://www.facebook.com/PortCityLK" target="_blank">
                    <img src="/assets/images/navigations/facebook.svg" alt="">
                </a>
            </div>

            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.youtube.com/channel/UCu-RLmtH6Tm1Y_AbmvFuFog/featured">
                    <img src="/assets/images/navigations/icon.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.instagram.com/portcitycolombo/">
                    <img src="/assets/images/navigations/insta.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.linkedin.com/company/port-city-colombo/">
                    <img src="/assets/images/navigations/linkedin.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://twitter.com/PortCityColombo">
                    <img src="/assets/images/navigations/twitter.svg" alt="">
                </a>
            </div>

        </div>
    </div>

    <div class="menu-wrapper">
        <div class="menu">
            <div class="data">
                <ul>
                    <li><a class="nav-data" href="/">HOME</a></li>
                    <li><a class="nav-data" href="/about">ABOUT PCC</a></li>
                    <li><a class="nav-data" href="/competition-one">THE COMPETITION</a></li>
                    <li><a class="nav-data" href="/media">MEDIA</a></li>
                    <li><a class="nav-data" href="/faq">FAQ</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div id="loader-wrapper">
        <div id="loader">
            <img src="/assets/images/navigations/logo.svg" alt="">
        </div>

        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>

    </div>






    <div class="mobile-navigation-wrap">
        <div class="mobile-navigation">
            <div class="mobile-logo">
                <a href="/"> <img src="/assets/images/navigations/logo.svg" alt=""> </a>
            </div>
            <div class="mobile-text">
                <span>PORT CITY COLOMBO <br>
                    STUDENT DESIGN COMPETITION</span>
            </div>
            <div class="hamberger">
                <img class="ham-ico" src="/assets/images/navigations/hamberger.svg" alt="">
            </div>
        </div>
    </div>

    <section class="about-section middel-sec">
        <div id="about-banner-video" class="bg-section">
            <div class="inner-container">
                <div class="short-nav">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="#">About PCC</a>
                        </li>
                    </ol>
                </div>

                <h3 class="main-banner-tittle-ani">ABOUT PORT CITY COLOMBO
                </h3>

                <div class="video-section">

                    <video id="vid1" poster="/assets/images/about/video.jpg" class="video-js vjs-default-skin"
                        controls="controls" width="640" height="264"
                        data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "https://youtu.be/sn2hlo7TmtA"}] }'></video>



                </div>
            </div>
        </div>

        <div id="about-concept" class="ab-sec bg-section">
            <div class="inner-container">
                <div class="concept-wrap">
                    <div class="img-sec txt">
                        <img class="pContent" src="/assets/images/about/porcity-concept.jpg" alt="">
                        <div class="white-panel"></div>
                    </div>
                    <div class="content-sec">
                        <h2 class="text-uppercase">The Vision</h2>
                        <p class="regular-p">
                            Strategically located at the epicentre of South Asia, Port City, Colombo offers access to a
                            thriving region poised for future expansion as a key link between Asia and the West.
                        </p>
                        <p class="regular-p">
                            The vision of the project is to convert Port City Colombo, to an exemplar city providing the
                            highest quality commercial, entertainment, medical, education and lifestyle opportunities.
                            It will be recognised as a catalyst for growth, a place that fuses the culture and energy of
                            a nation with international best practice.
                        </p>
                        <p class="regular-p">
                            Once this spectacular megapolis converts to reality by the year 2030; the city will home to
                            a third of Sri Lanka’s population and projected to contribute over 40% of the nation’s
                            economy, transforming Sri Lanka into a high-income global player in logistics, manufacturing
                            and IT, getting us closer to the objective of transitioning Sri Lanka to a developed nation.
                        </p>

                        <a href="https://www.portcitycolombo.lk/about">
                            <button class="btn btn-primary main-btn text-uppercase">read more</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div id="porpes" class="bg-section">
            <div class="inner-container">
                <div class="concept-wrap">

                    <div class="content-sec">
                        <h2 class="text-uppercase">The Competition</h2>
                        <p class="regular-p">
                            The competition is professionally curated to the highest standards, and it is overseen by an
                            independent panel of industry experts, who are highly celebrated in the world of design and
                            architecture. By partaking in this, the youth will be inspired to dig deep into their core
                            creative nature and compete in a high profiled competition, beside top-tier industry
                            professionals who will be guiding and grooming the youngsters to tap into their true
                            potential.
                        </p>
                        <p class="regular-p">
                            The ultimate goal of the competition is to make you, our valued citizens, a part of this
                            great movement in making a difference in the future of Sri Lanka. That’s why we collaborate
                            with our young, the real future of our world, to see the light of Port City Colombo and
                            envision a future that is a blend of innovation, creativity and sustainable living.
                        </p>

                    </div>


                    <div class="img-sec txt">
                        <img class="" src="/assets/images/about/porcity-concept2.jpg" alt="">
                        <div class="white-panel"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="port-sub-footer">
            <img class="trans" src="/assets/images/about/trans-sub-footer.png" alt="">
            <div class="wrap-colm-sec">
                <div class="port-colombo">
                    <div class="wrap-sec">
                        <img src="/assets/images/competition/comp-logo.svg" alt="">
                        <span>PORT CITY COLOMBO</span>
                    </div>
                </div>
                <div class="port-dis-link">
                    <p>
                        To learn more, please visit to,
                    </p>
                    <a href="https://www.portcitycolombo.lk/" target="_blank">portcitycolombo.lk</a>
                </div>
            </div>
        </div>
    </section>

    <div class="middel-sec">
        <footer>
            <div class="wrap-footer">
                <div class="port-logo">
                    <img src="/assets/images/footer/port-footer.png" alt="">
                </div>
                <div class="ft-dis">
                    <p>©2020. PCC Student Design Competition 2020 </p>
                    <p class="solution">Solution By <a href="https://www.saberion.com/" target="_blank">SABERION</a></p>
                    <div class="social">

                    </div>
                </div>

                <div class="footer-social-link">
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/facebook.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/icon.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/insta.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/linkedin.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/twitter.svg" alt="">
                    </a>
                </div>
            </div>
        </footer>
    </div>
    <div class="right-menu">
        <div class="hamberger">
            <img class="ham-ico" src="/assets/images/navigations/hamberger.svg" alt="">
        </div>

        <div class="slider-bulet">
            <div class="swiper-pagination"></div>
        </div>

        <div class="chat-section">

            <div class="text-ani-inner port-right-logo">
                <a href="#">
                    <img src="/assets/images/navigations/port-logo.svg" alt="">
                </a>

            </div>

            <!--Start of Tawk.to Script-->
            <script type="text/javascript">
                var Tawk_API = Tawk_API || {},
                    Tawk_LoadStart = new Date();
                (function () {
                    var s1 = document.createElement("script"),
                        s0 = document.getElementsByTagName("script")[0];
                    s1.async = true;
                    s1.src = 'https://embed.tawk.to/5f227d964f3c7f1c910d7fdc/default';
                    s1.charset = 'UTF-8';
                    s1.setAttribute('crossorigin', '*');
                    s0
                        .parentNode
                        .insertBefore(s1, s0);
                })();
            </script>
            <!--End of Tawk.to Script-->
        </div>
    </div>
    <script>
        var siteBaseUrl = '';
    </script>
    <script src="/assets/js/lib.js"></script>
    <script src="/assets/js/main.js"></script>
</body>

</html>