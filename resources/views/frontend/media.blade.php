<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Envision</title>
    <link rel="stylesheet" href="/assets/css/lib.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="async" src="https://www.googletagmanager.com/gtag/js?id=UA-174141483-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-174141483-1');
    </script>

</head>

<body class="onload aod-media">
    <div class="left-menu">
        <div class="logo">
            <a href="/">
                <img src="/assets/images/navigations/logo.svg" alt="">
            </a>
        </div>
        <div class="pcc-sec lg1">
            <span>PCC STUDENT DESIGN COMPETITION 2020</span>
        </div>
        <div class="social-media">
            <div class="text-ani-inner">
                <a class="lgz3" href="https://www.facebook.com/PortCityLK" target="_blank">
                    <img src="/assets/images/navigations/facebook.svg" alt="">
                </a>
            </div>

            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.youtube.com/channel/UCu-RLmtH6Tm1Y_AbmvFuFog/featured">
                    <img src="/assets/images/navigations/icon.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.instagram.com/portcitycolombo/">
                    <img src="/assets/images/navigations/insta.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://www.linkedin.com/company/port-city-colombo/">
                    <img src="/assets/images/navigations/linkedin.svg" alt="">
                </a>
            </div>
            <div class="text-ani-inner" target="_blank">
                <a class="lgz3" href="https://twitter.com/PortCityColombo">
                    <img src="/assets/images/navigations/twitter.svg" alt="">
                </a>
            </div>

        </div>
    </div>

    <div class="menu-wrapper">
        <div class="menu">
            <div class="data">
                <ul>
                    <li><a class="nav-data" href="/">HOME</a></li>
                    <li><a class="nav-data" href="/about">ABOUT PCC</a></li>
                    <li><a class="nav-data" href="/competition-one">THE COMPETITION</a></li>
                    <li><a class="nav-data" href="/media">MEDIA</a></li>
                    <li><a class="nav-data" href="/faq">FAQ</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div id="loader-wrapper">
        <div id="loader">
            <img src="/assets/images/navigations/logo.svg" alt="">
        </div>

        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>

    </div>






    <div class="mobile-navigation-wrap">
        <div class="mobile-navigation">
            <div class="mobile-logo">
                <a href="/"> <img src="/assets/images/navigations/logo.svg" alt=""> </a>
            </div>
            <div class="mobile-text">
                <span>PORT CITY COLOMBO <br>
                    STUDENT DESIGN COMPETITION</span>
            </div>
            <div class="hamberger">
                <img class="ham-ico" src="/assets/images/navigations/hamberger.svg" alt="">
            </div>
        </div>
    </div>

    <section class="middel-sec">
        <div class="wrap-over">
            <div class="video-section bg-section">
                <div class="inner-container">
                    <div class="video-wrapper-main">
                        <div class="short-nav">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    <a href="#">Media</a>
                                </li>
                            </ol>
                        </div>
                        <h3>MEDIA</h3>

                        <div class="title-header">
                            <img src="/assets/images/media/video.svg" alt="">
                            <h2>VIDEOS</h2>
                        </div>

                        <div class="video-slider">


                            <div class="video-slide">
                                <div class="swiper-wrapper">

                                    <div class="swiper-slide">
                                        <div class="vid">
                                            <iframe width="100%" height="300"
                                                src="https://www.youtube.com/embed/-USCb7TcQo4" frameborder="0"
                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>

                                        </div>
                                        <div class="content">
                                            <p>Meet Rochelle - Top 03 finalist ‘Dream’ Beach Sculpture Competition</p>
                                        </div>
                                    </div>

                                    <div class="swiper-slide">
                                        <div class="vid">
                                            <iframe width="100%" height="300" src="https://www.youtube.com/embed/j6DDwLOBgyU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                        </div>
                                        <div class="content">
                                            <p>Meet Lakal - Top 03 finalist of the ‘Dream’ Beach Sculpture Competition</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="vid">
                                            <iframe width="100%" height="300" src="https://www.youtube.com/embed/4sAOsbAh8OI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                        </div>
                                        <div class="content">
                                            <p>Meet Pulasthi, Viranga and Tharindu - Top 03 finalist of the ‘Dream’ Beach Sculpture Competition</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="vid">
                                            <iframe width="100%" height="300" src="https://www.youtube.com/embed/ibb8ICSJCAw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                        </div>
                                        <div class="content">
                                            <p>Meet Ranjaka - Top 03 finalist of the ‘Vitality' Interactive Wall Competition</p>
                                        </div>
                                    </div>

                                    <div class="swiper-slide">
                                        <div class="vid">
                                            <iframe width="100%" height="300" src="https://www.youtube.com/embed/n9R43_W9fpM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                        </div>
                                        <div class="content">
                                            <p>Meet Nethmini, Azamul & Shavendra - Top 03 finalist of the ‘Vitality' Interactive Wall Competition</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="vid">
                                            <iframe width="100%" height="300" src="https://www.youtube.com/embed/vqZ3TwJu7OI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                        </div>
                                        <div class="content">
                                            <p>Meet R. Javaneesan - Top 03 finalist of the ‘Vitality' Interactive Wall Competition</p>
                                        </div>
                                    </div>


                                    <div class="swiper-slide">
                                        <div class="vid">
                                            <iframe width="100%" height="300"
                                                src="https://www.youtube.com/embed/ywtBQ9bfY0g" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen="allowfullscreen"></iframe>

                                        </div>
                                        <div class="content">
                                            <p>Corporate Video | Port City Colombo</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="vid">

                                            <iframe width="100%" height="300"
                                                src="https://www.youtube.com/embed/EdajYG3bTxY" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen="allowfullscreen"></iframe>

                                        </div>
                                        <div class="content">
                                            <p>Port City will be one of the catalysts to make Sri Lanka the center of
                                                change in South Asia</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="vid">

                                            <iframe width="100%" height="300"
                                                src="https://www.youtube.com/embed/E24CSIuMuIU" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen="allowfullscreen"></iframe>

                                        </div>
                                        <div class="content">
                                            <p>10 Things You Should Know About Port City Colombo</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="vid">

                                            <iframe width="100%" height="300"
                                                src="https://www.youtube.com/embed/k4iouM6SNIQ" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen="allowfullscreen"></iframe>

                                        </div>
                                        <div class="content">
                                            <p>Grand fireworks display | Port City Colombo</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="pre-section swiper-button-prev"></div>
                            <div class="next-section play-btn-right"></div>
                        </div>

                        <div class="document-section">
                            <div class="title-header">
                                <img src="/assets/images/media/news.svg" alt="">
                                <h2>IMPORTANT DOCUMENTS</h2>
                            </div>

                            <div class="doc-slider">
                                <div class="doc-slide">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="doc">
                                                <h5>Intellectual Property Release</h5>
                                                <div class="bottom-btn-sec">
                                                    <span>PDF / 84kb</span>
                                                    <a href="/assets/pdf/Intellectual_Property_Release_Form_Team_-_Sculpture_Competition.docx"
                                                        download>
                                                        <button class="btn main-btn">DOWNLOAD</button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="swiper-slide">
                                            <div class="doc">
                                                <h5>Rules & Guidelines</h5>
                                                <div class="bottom-btn-sec">
                                                    <span>PDF / 84kb</span>
                                                    <a href="/assets/pdf/Rules&Guidelines.pdf" download>
                                                        <button class="btn main-btn">DOWNLOAD</button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="pre-section swiper-button-doc-prev"></div>
                                <div class="next-section play-btn-doc-right"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-container">
                <div id="gallery-section" class="gallary-section">
                    <div class="title-header">
                        <img src="/assets/images/media/camara.svg" alt="">
                        <h2 class="text-uppercase">Winning Designs</h2>
                    </div>

                    <div class="gallery-wrp">
                        <div class="gal-item" style="align-items: flex-start">
                            <div class="gb-wrp">
                                <div class="gallery-inside">
                                    <img src="/assets/images/media/gallary-inside/media-gal1.jpg" alt="">
                                </div>
                            </div>

                            <div class="content">
                                <p class="text-capitalize">Winner - Beach Sculpture</p>
                                <ul>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> tharindu perera</p></li>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> pulasthi handunge & viranga waduge</p></li>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> city school of architecture</p></li>
                                </ul>
                            </div>
                        </div>
                        <div class="gal-item" style="align-items: flex-start">
                            <div class="gb-wrp">
                                <div class="wht-bg">
                                    <div class="gallery-inside">
                                        <img src="assets/images/media/gallary-inside/PCCSDC255.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <p class="text-capitalize">Winner - Interactive wall</p>
                                <ul>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> r javaneesam</p></li>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> university of moratuwa</p></li>
                                    
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="gallery-wrp">
                        <div class="gal-item" style="align-items: flex-start">
                            <div class="gb-wrp">
                                <div class="gallery-inside">
                                    <img src="/assets/images/media/gallary-inside/media-gal4.jpg" alt="">
                                </div>
                            </div>

                            <div class="content">
                                <p class="text-capitalize">runner up - beach Sculpture</p>
                                <ul>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> rochelle frenando</p></li>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> academy of design</p></li>
                                </ul>
                            </div>
                        </div>
                        <div class="gal-item" style="align-items: flex-start">
                            <div class="gb-wrp">
                                <div class="wht-bg">
                                    <div class="gallery-inside">
                                        <img src="/assets/images/media/gallary-inside/PCCSDC77.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <p class="text-capitalize">runner up - interactive wall </p>
                                <ul>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> nethmini liyanagamage-manchester uni</p></li>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> shavendra goonetilleke-melbourne uni</p></li>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> azamul huq abdul haleem-knet uni</p></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="gallery-wrp">
                        <div class="gal-item" style="align-items: flex-start">
                            <div class="gb-wrp">
                                <div class="gallery-inside">
                                    <img src="/assets/images/media/gallary-inside/media-gal7.jpg" alt="">
                                </div>
                            </div>

                            <div class="content">
                                <p class="text-capitalize">runner up - beach sculputre </p>
                                <ul>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> lakal piyarathna</p></li>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> university of moratuwa</p></li>
                                </ul>
                            </div>
                        </div>
                        <div class="gal-item" style="align-items: flex-start">
                            <div class="gb-wrp">
                                <div class="wht-bg">
                                    <div class="gallery-inside">
                                        <img src="/assets/images/media/gallary-inside/PCCSDC199.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <p class="text-capitalize">runner up - interactive wall  </p>
                                <ul>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> ranjaka kalhara hettiarachchi</p></li>
                                    <li> <p style="font-size: 18px; line-height: 26px;" class="text-capitalize"> livepool john moores university</p></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    {{-- <div class="gallery-wrp">
                        <div class="gal-item">
                            <div class="gb-wrp">
                                <div class="gallery-inside">
                                    <img src="/assets/images/media/gallary-inside/PCCSDC77.jpg" alt="">
                                </div>
                            </div>

                            <div class="content">
                                <p>PCCSDC77</p>
                            </div>
                        </div>
                        <div class="gal-item">
                            <div class="gb-wrp">
                                <div class="wht-bg">
                                    <div class="gallery-inside">
                                        <img src="/assets/images/media/gallary-inside/PCCSDC260.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <p>PCCSDC260</p>
                            </div>
                        </div>

                    </div>
                    <div class="gallery-wrp">
                        <div class="gal-item">
                            <div class="gb-wrp">
                                <div class="gallery-inside">
                                    <img src="/assets/images/media/gallary-inside/PCCSDC159.jpg" alt="">
                                </div>
                            </div>

                            <div class="content">
                                <p>PCCSDC159</p>
                            </div>
                        </div>
                        <div class="gal-item">
                            <div class="gb-wrp">
                                <div class="wht-bg">
                                    <div class="gallery-inside">
                                        <img src="/assets/images/media/gallary-inside/PCCSDC171.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <p>PCCSDC171</p>
                            </div>
                        </div>

                    </div>
                    <div class="gallery-wrp">
                        <div class="gal-item">
                            <div class="gb-wrp">
                                <div class="gallery-inside">
                                    <img src="/assets/images/media/gallary-inside/PCCSDC199.jpg" alt="">
                                </div>
                            </div>

                            <div class="content">
                                <p>PCCSDC199</p>
                            </div>
                        </div>
                        <div class="gal-item">
                            <div class="gb-wrp">
                                <div class="wht-bg">
                                    <div class="gallery-inside">
                                        <img src="/assets/images/media/gallary-inside/PCCSDC229.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <p>PCCSDC229</p>
                            </div>
                        </div>

                    </div>
                    <div class="gallery-wrp">
                        <div class="gal-item">
                            <div class="gb-wrp">
                                <div class="gallery-inside">
                                    <img src="/assets/images/media/gallary-inside/PCCSDC255.jpg" alt="">
                                </div>
                            </div>

                            <div class="content">
                                <p>PCCSDC255</p>
                            </div>
                        </div>
                        <div class="gal-item">
                            <div class="gb-wrp">
                                <div class="wht-bg">
                                    <div class="gallery-inside">
                                        <img src="/assets/images/media/gallary-inside/PCCSDC257.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <p>PCCSDC257</p>
                            </div>
                        </div>

                    </div> --}}
                </div>
            </div>
        </div>

    </section>

    <div class="middel-sec">
        <footer>
            <div class="wrap-footer">
                <div class="port-logo">
                    <img src="/assets/images/footer/port-footer.png" alt="">
                </div>
                <div class="ft-dis">
                    <p>©2020. PCC Student Design Competition 2020 </p>
                    <p class="solution">Solution By <a href="https://www.saberion.com/" target="_blank">SABERION</a></p>
                    <div class="social">

                    </div>
                </div>

                <div class="footer-social-link">
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/facebook.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/icon.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/insta.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/linkedin.svg" alt="">
                    </a>
                    <a class="lgz3" href="#">
                        <img src="/assets/images/navigations/twitter.svg" alt="">
                    </a>
                </div>
            </div>
        </footer>
    </div>
    <div class="right-menu">
        <div class="hamberger">
            <img class="ham-ico" src="/assets/images/navigations/hamberger.svg" alt="">
        </div>

        <div class="slider-bulet">
            <div class="swiper-pagination"></div>
        </div>

        <div class="chat-section">

            <div class="text-ani-inner port-right-logo">
                <a href="#">
                    <img src="/assets/images/navigations/port-logo.svg" alt="">
                </a>

            </div>

            <!--Start of Tawk.to Script-->
            <script type="text/javascript">
                var Tawk_API = Tawk_API || {},
                    Tawk_LoadStart = new Date();
                (function () {
                    var s1 = document.createElement("script"),
                        s0 = document.getElementsByTagName("script")[0];
                    s1.async = true;
                    s1.src = 'https://embed.tawk.to/5f227d964f3c7f1c910d7fdc/default';
                    s1.charset = 'UTF-8';
                    s1.setAttribute('crossorigin', '*');
                    s0
                        .parentNode
                        .insertBefore(s1, s0);
                })();
            </script>
            <!--End of Tawk.to Script-->
        </div>
    </div>
    <script>
        var siteBaseUrl = '';
    </script>
    <script src="/assets/js/lib.js"></script>
    <script src="/assets/js/main.js"></script>
</body>

</html>