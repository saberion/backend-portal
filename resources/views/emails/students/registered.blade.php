<p>Dear Entrant/s,</p>

<b style="color: red">Note</b> - this email is valid ONLY for 1 hour from the time of receival.

<ul>
    <li>If you're reading this email, then you're a few seconds away from completing the registration process.</li>
    <li>Please click on the following link to verify this email address and you'll be taken to the login page to login
        to the portal.</li>
    <li>After logged in please update your profile/s if necessary and finish the submission of the design.</li>
</ul>

<a href="{{ $url }}">Verify this email address</a>
