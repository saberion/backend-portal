@php
    $student = auth()->guard('student')->user();

    $avatar = ! empty($student->avatar) ? Storage::url($student->avatar)
        : asset('images/profile.png');
@endphp

<div class="login">
    <div class="login-left">
        <span>{{ $student->name }}</span>
        <span>{{ $entry->identity }}</span>
    </div>

    <div class="login-img">
        <img src="{{ $avatar }}" alt="">
    </div>

    <div class="logout">
        <form action="{{ route('students.logout') }}" method="POST">
            @csrf

            <button type="submit">Logout</button>
        </form>
    </div>
</div>