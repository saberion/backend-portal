@extends('layouts.app')

@section('title', 'Send Reset Link')

@section('content')
    <div class="main-wrapper">
        <div class="inner-wrapper">
            <div class="top-part">
                @include('partials.head', [
                    'title' => 'SEND RESET LINK',
                    'description' => 'In order to submit your work, you have to be a registered user.'
                ])

                <form action="{{ route('students.password.email') }}" method="POST">
                    @csrf

                    <div class="contain-inputs">
                        <div class="input-cover left-section">
                            <div class="input-field">
                                <input type="email" name="email" value="{{ old('email') }}" id="email"
                                    class="validate" autocomplete="email" required>
                                <label for="email">Email *</label>

                                @error('email')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>

                            <button type="submit" class="waves-effect waves-light btn">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    @include('layouts.alerts')
@endsection