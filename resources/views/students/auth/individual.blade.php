@php
    $years = [2017, 2018, 2019, 2020, 2021, 2022];
@endphp

@extends('layouts.app')

@section('title', 'Student Registration')

@section('content')
    <div class="main-wrapper">
        <div class="full-contact-container">
            <div class="spacer">
                @include('partials.back', ['url' => route('root')])

                @include('partials.head', [
                    'title' => 'SUBMISSION PORTAL',
                    'description' => 'In order to submit your work, you have to be a registered user.'
                ])

                <form action="{{ route('students.register') }}" method="POST">
                    @csrf

                    <div class="wrap-input-ele">
                        <div class="lft-sec">
                            <div class="input-field">
                                <input type="text" name="students[0][name]" value="{{ old('students.0.name') }}"
                                    id="register_name" class="validate"  autocomplete="name" required>
                                <label for="register_name">Name *</label>

                                @error('students.0.name')
                                    <span class="helper-text" data-error="The name field is required.">The name field is
                                        required.</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="text" name="students[0][university]" value="{{ old('students.0.university') }}"
                                    id="register_uni" class="validate" required>
                                <label for="register_uni">What is your university *</label>

                                @error('students.0.university')
                                    <span class="helper-text" data-error="The university field is required.">The university
                                        field is required.</span>
                                @enderror
                            </div>

                            <div class="label-sec">
                                <label>
                                    <input type="radio" name="students[0][education]" value="undergraduate"
                                        class="validate" {{ old('students.0.education') === "undergraduate" ? 'checked' : '' }}
                                        required>
                                    <span>Undergraduate</span>
                                </label>

                                <label>
                                    <input type="radio" name="students[0][education]" value="graduate" class="validate"
                                        {{ old('students.0.education') === "graduate" ? 'checked' : '' }} required>
                                    <span>Graduate</span>
                                </label>
                            </div>

                            <div class="input-field">
                                <input type="text" name="students[0][course]" value="{{ old('students.0.course') }}"
                                    id="register_uni_folow" class="validate">
                                <label for="register_uni_folow">Course you are following</label>

                                @error('students.0.course')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="email" name="students[0][email]" value="{{ old('students.0.email') }}"
                                    id="register_email_format" class="validate" autocomplete="email" required>
                                <label for="register_email_format">Email Address *</label>

                                @error('students.0.email')
                                    <span class="helper-text" data-error="The email field is required.">The email field is
                                        required.</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="password" name="students[0][password]" id="register_pre-pass" class="validate"
                                    autocomplete="new-password" minlength="8" required>
                                <label for="register_pre-pass">Preferred Password *</label>

                                @error('students.0.password')
                                    <span class="helper-text" data-error="The password field is required.">The password field is
                                        required.</span>
                                @enderror
                            </div>
                        </div>

                        <div class="lft-sec">
                            <div class="input-field">
                                <input type="text" name="students[0][nic]" value="{{ old('students.0.nic') }}"
                                    id="register_nic" class="validate" minlength="10" required>
                                <label for="register_nic">NIC as 111111111V or X *</label>

                                @error('students.0.nic')
                                    <span class="helper-text" data-error="The nic field is required.">The nic field is
                                        required.</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="text" name="students[0][registration]"
                                    value="{{ old('students.0.registration') }}" id="register_uni_num" class="validate" required>
                                <label for="register_uni_num" >University Student Number *</label>

                                @error('students.0.registration')
                                    <span class="helper-text" data-error="The registration field is required.">The registration
                                        field is required.</span>
                                @enderror
                            </div>

                            <div class="label-sec">
                                <label>
                                    <input type="radio" name="students[0][education]" value="postgraduate"
                                        class="validate" {{ old('students.0.education') === "postgraduate" ? 'checked' : '' }} required>
                                    <span>Postgraduate</span>
                                </label>
                            </div>

                            <div class="input-field">
                                <select class="browser-default" name="students[0][year]" class="validate">
                                    <option value="" disabled selected>Year of Study</option>
                                    @foreach ($years as $year)
                                        <option value="{{ $year }}">{{ $year }}</option>
                                    @endforeach
                                </select>

                                @error('students.0.year')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="tel" name="students[0][contact]" value="{{ old('students.0.contact') }}"
                                    id="register_cnt-num" class="validate" minlength="12" autocomplete="mobile">
                                <label for="register_cnt-num">Contact Number as +94111111111</label>

                                @error('students.0.contact')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="password" name="students[0][password_confirmation]" id="register_pass"
                                    class="validate" autocomplete="new-password" minlength="8" required>
                                <label for="register_pass">Confirm Password *</label>
                            </div>
                        </div>

                        <div class="lft-sec">
                            <button class="waves-effect waves-light btn">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('layouts.alerts')
@endsection
