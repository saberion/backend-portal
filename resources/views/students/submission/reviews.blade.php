@extends('layouts.app')

@section('title', 'Reviews')

@section('content')
    <div class="main-wrapper">
        @include('students.auth.logout', ['entry' => session('entry')])

        <div class="full-contact-container">
            <div class="spacer">
                @include('partials.head', ['title' => 'COMPETITION'])
            </div>

            <div class="containre-sec">
                <ul class="progressbar">
                    <li class="active">
                        <img src="{{ asset('images/tick.png') }}" alt="">
                        Submission
                    </li>

                    <li class="active">
                        <img src="{{ asset('images/tick.png') }}" alt="">
                        Result
                    </li>
                </ul>
            </div>

            @if ($entry->submissions->isNotEmpty())
                @foreach ($entry->submissions as $submission)
                    <table class="striped highlight responsive-table">
                        <thead>
                            <tr>
                                <th>Points</th>
                                <th>Comments</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if ($submission->reviews->isNotEmpty())
                                @foreach ($submission->reviews as $review)
                                    <tr>
                                        <td>{{ $review->points }}</td>
                                        <td>{{ $review->comments }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                @endforeach
            @endif
        </div>
    </div>
@endsection