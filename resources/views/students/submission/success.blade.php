@extends('layouts.app')

@section('title', 'Student Submission')

@section('content')
    <div class="main-wrapper">
        @include('students.auth.logout', ['entry' => session('entry')])

        <div class="full-contact-container">
            <div class="spacer">
                @include('partials.head', ['title' => 'COMPETITION'])
            </div>

            <div class="containre-sec">
                <ul class="progressbar">
                    <li class="active">
                        <img src="{{ asset('images/tick.png') }}" alt="">
                        Submission
                    </li>
                    <li>Result</li>
                </ul>
            </div>

            <h2>Congrats! You have successfully submitted your design.</h2>

            <h5>The Jury will carefully go through your proposel and the design soon.</h5>

            <h5>Get in touch with us, we will update you soon on the results.</h5>
        </div>
    </div>

    @include('layouts.alerts')
@endsection