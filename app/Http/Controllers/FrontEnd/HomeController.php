<?php

namespace App\Http\Controllers\FrontEnd;

use Instagram\Api;
use App\Http\Controllers\Controller;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class HomeController extends Controller
{
    /**
     * Show homepage.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $medias = $this->getInstagramMedias();

        return view('frontend.index', compact('medias'));
    }

    /**
     * Get instagram media.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getInstagramMedias()
    {
        if (cache()->has('instagram')) {
            return cache('instagram');
        }

        try {
            $api = new Api(new FilesystemAdapter('Instagram'));

            $api->login(
                config('services.instagram.username'),
                config('services.instagram.password')
            );

            $profile = $api->getProfile(config('services.instagram.username'));

            $medias = collect($profile->getMedias())->take(10);

            cache(['instagram' => $medias], now()->addHour());

            return $medias;
        } catch (\Throwable $th) {
            return collect();
        }
    }
}
