<?php

namespace App\Http\Controllers\FrontEnd;

use App\Models\Result;
use App\Http\Controllers\Controller;

class CompetitionTwoController extends Controller
{
    /**
     * Show competition two page
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $results = Result::whereHas('submission', function ($query) {
            $query->where(['competition' => 1]);
        })->with([
            'submission:id,entry_id',
            'submission.entry:id',
            'submission.entry.students:id,name,university,avatar'
        ])->limit(3)->get(['id', 'submission_id']);

        return view('frontend.competitionTwo', compact('results'));
    }
}
