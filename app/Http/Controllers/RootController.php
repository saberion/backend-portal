<?php

namespace App\Http\Controllers;

class RootController extends Controller
{
    /**
     * Show login root.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('root');
    }
}
