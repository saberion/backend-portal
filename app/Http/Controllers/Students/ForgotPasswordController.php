<?php

namespace App\Http\Controllers\Students;

use App\Models\Student;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordResetRequested;

class ForgotPasswordController extends Controller
{
    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\View\View
     */
    public function showLinkRequestForm()
    {
        return view('students.auth.email');
    }

    /**
     * Send a reset link to the given user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetLinkEmail()
    {
        $validated = request()->validate(['email' => 'required|email']);

        $student = Student::where(['email' => $validated['email']])->first(['id']);

        if (! $student) {
            return back()->with([
                'error' => "We can't find a student with that email address."
            ]);
        }

        Mail::to($validated['email'])->send(new PasswordResetRequested(
            $student
        ));

        return redirect()->route('root')->with([
            'success' => 'Password reset link sent. Please check your inbox.'
        ]);
    }
}
