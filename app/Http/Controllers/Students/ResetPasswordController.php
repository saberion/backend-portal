<?php

namespace App\Http\Controllers\Students;

use App\Models\Student;
use App\Http\Controllers\Controller;
use App\Http\Requests\Students\ResetPassword;

class ResetPasswordController extends Controller
{
    /**
     * Display the password reset form.
     *
     * @param  mixed $id
     * @return \Illuminate\View\View
     */
    public function showResetForm($id)
    {
        $student = Student::find($id, ['email']);

        if (! $student) {
            return back()->with(['error', "We couldn't find your account."]);
        }

        return view('students.auth.reset')->with([
            'id'    => $id,
            'email' => $student->email
        ]);
    }

    /**
     * Reset the given user's password.
     *
     * @param  ResetPassword $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(ResetPassword $request)
    {
        $validated = $request->validated();

        $student = Student::find(request('id'));

        if (! $student || $validated['email'] !== $student->email) {
            return back()->with(['error' => "We couldn't find your account."]);
        }

        $student->password = $validated['password'];

        $student->save();

        return redirect()->route('root')->with([
            'success' => 'Password reset. Please login.'
        ]);
    }
}
