<?php

namespace App\Http\Controllers\Students;

use App\Models\Lock;
use App\Http\Controllers\Controller;
use App\Http\Requests\Students\Submission;

class SubmissionController extends Controller
{
    /**
     * Show submission upload form.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if ($this->isLocked()) {
            return redirect()->route('students.submission.reviews');
        }

        if ($this->isSubmitted()) {
            return view('students.submission.success');
        }

        return back()->with('error', 'Submissions closed.');

        // return view('students.submission.index');
    }

    /**
     * Check whether both competitions are locked.
     *
     * @return bool
     */
    public function isLocked()
    {
        return Lock::where(['competition' => 1])->exists() &&
            Lock::where(['competition' => 2])->exists();
    }

    /**
     * Check whether submissions are uploaded for both competitions.
     *
     * @return bool
     */
    public function isSubmitted()
    {
        $entry = session('entry');

        return $entry->competition(1)->exists() && $entry->competition(2)
            ->exists();
    }

    /**
     * Store submission.
     *
     * @param  Submission $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Submission $request)
    {
        return back()->with('error', 'Submissions closed.');

        if (! session()->has('entry')) {
            abort(404);
        }

        if ($this->isSubmittedForCompetition()) {
            return back()->with('error', 'Already submitted.');
        }

        $values = $this->storeDocuments($request);

        session('entry')->submissions()->create($values);

        return redirect()->route('students.submission.success')
            ->with('success', 'Submission completed.');
    }

    /**
     * Check whether a submission exists for the incoming competition.
     *
     * @return bool
     */
    public function isSubmittedForCompetition()
    {
        $entry = session('entry');

        if (request('competition') == 1 && $entry->competition(1)->exists()) {
            return true;
        }

        if (request('competition') == 2 && $entry->competition(2)->exists()) {
            return true;
        }

        return false;
    }

    /**
     * Store submission documents.
     *
     * @param  Submission $request
     * @return array $values
     */
    public function storeDocuments(Submission $request)
    {
        $values = $request->except(['terms']);

        $values['proposal'] = $request->proposal
            ->store('documents/submissions', ['disk' => 'public']);

        $values['design'] = $request->design
            ->store('documents/submissions', ['disk' => 'public']);

        return $values;
    }

    /**
     * Show submission success message.
     *
     * @return \Illuminate\View\View
     */
    public function success()
    {
        return view('students.submission.success');
    }

    /**
     * Show reviews of the submissions.
     *
     * @return \Illuminate\View\View
     */
    public function reviews()
    {
        if (! $this->isLocked()) {
            return redirect()->route('students.submission.index')
                ->with(['error' => 'Competitions are not locked yet']);
        }

        if (! session()->has('entry')) {
            abort(404);
        }

        $entry = session('entry');

        $entry->load([
            'submissions:id,entry_id',
            'submissions.reviews:id,submission_id,points,comments'
        ]);

        return view('students.submission.reviews', compact('entry'));
    }
}
