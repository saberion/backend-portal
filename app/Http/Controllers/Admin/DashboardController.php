<?php

namespace App\Http\Controllers\Admin;

use App\Models\Entry;
use App\Models\Review;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Show admin dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $entries = $this->getEntrants();

        return view('admin.dashboard.index', compact('entries'));
    }

    /**
     * Get paginated entries.
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getEntrants()
    {
        if (request()->filled('competition') && request('competition')) {
            return Entry::whereHas('submissions', function ($query) {
                $query->where(['competition' => request('competition')]);
            })->withCount(['competitionOne', 'competitionTwo'])->paginate(15);
        }

        return Entry::withCount(['competitionOne', 'competitionTwo'])
            ->paginate(15);
    }

    /**
     * Delete entry & related students, submissions, reviews.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteEntry($id)
    {
        $entry = Entry::with(['students:id', 'submissions:id,entry_id'])
            ->findOrFail($id, ['id']);

        foreach ($entry->students as $student) {
            $student->delete();
        }

        foreach ($entry->submissions as $submission) {
            Review::where(['submission_id' => $submission->id])->delete();

            $submission->delete();
        }

        $entry->delete();

        return back()->with(['success', 'Deleted ' . $entry->identity]);
    }
}
