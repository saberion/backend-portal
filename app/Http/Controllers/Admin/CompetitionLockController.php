<?php

namespace App\Http\Controllers\Admin;

use App\Models\Lock;
use App\Models\Result;
use App\Models\JuryMember;
use App\Models\Submission;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;

class CompetitionLockController extends Controller
{
    /**
     * Show round lock.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $locks = Lock::all();

        return view('admin.competitionLock.index', compact('locks'));
    }

    /**
     * Calculate & store results of the given competition.
     *
     * @param  mixed $competition
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($competition)
    {
        if ($this->alreadyLocked($competition)) {
            return back()->with(['error' => 'Competition already locked']);
        }

        $juryCount = JuryMember::count();

        if ($this->unreviewedSubmissionsExists($competition, $juryCount)) {
            return back()->with(['error' => 'Unreviewed submissions exist']);
        }

        Lock::create(['competition' => $competition]);

        $submissions = $this->getSubmissionsWithReviews($competition);

        $results = $this->calculateResults($submissions, $juryCount);

        Result::insert($results->toArray());

        return back()->with(['success' => "Competition $competition locked"]);
    }

    /**
     * Check whether the given competition is already locked.
     *
     * @param  mixed $competition
     * @return bool
     */
    public function alreadyLocked($competition)
    {
        return Lock::where('competition', $competition)->exists();
    }

    /**
     * Check whether unreviewed submissions exist for the given competition.
     *
     * @param  mixed $competition
     * @param  int $juryCount
     * @return bool
     */
    public function unreviewedSubmissionsExists($competition, $juryCount)
    {
        return Submission::competition($competition)
            ->has('reviews', '<', $juryCount)->exists();
    }

    /**
     * Get submissions with reviews for the given competition.
     *
     * @param  mixed $competition
     * @return Collection
     */
    public function getSubmissionsWithReviews($competition)
    {
        return Submission::select(['id'])->competition($competition)
            ->with(['reviews:id,submission_id,points'])
            ->get();
    }

    /**
     * Calculate results of the given submissions.
     *
     * @param  Collection $submissions
     * @param  int $juryCount
     * @return \Illuminate\Support\Collection
     */
    public function calculateResults(Collection $submissions, $juryCount)
    {
        $results = collect();

        foreach ($submissions as $submission) {
            $results->push([
                'submission_id' => $submission->id,
                'average_points' => $submission->reviews->sum('points') / $juryCount
            ]);
        }

        return $results->sortByDesc('average_points')->take(20);
    }

    /**
     * Show results of the given competition.
     *
     * @param  mixed $competition
     * @return \Illuminate\View\View
     */
    public function results($competition)
    {
        $submissions = $this->getSubmissionsWithResults($competition);

        return view(
            'admin.competitionLock.results',
            compact('submissions', 'competition')
        );
    }

    /**
     * Get submissions with results for the given competition.
     *
     * @param  mixed $competition
     * @return Collection
     */
    public function getSubmissionsWithResults($competition)
    {
        $submissions = Submission::select(['id', 'entry_id', 'proposal', 'design'])
            ->competition($competition)->has('result')
            ->with([
                'entry:id,identity',
                'result:id,submission_id,average_points',
                'reviews:id,jury_member_id,submission_id,comments',
                'reviews.juryMember:id,name'
            ])
            ->get();

        return $submissions->sortByDesc('result.average_points');
    }
}
