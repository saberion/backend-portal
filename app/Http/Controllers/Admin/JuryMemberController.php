<?php

namespace App\Http\Controllers\Admin;

use App\Models\Review;
use App\Models\JuryMember;
use App\Models\Submission;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreJuryMember;

class JuryMemberController extends Controller
{
    /**
     * Show jury members.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $submissionsCount = Submission::count();
        $juryMembers = JuryMember::withTrashed()->withCount('reviews')
            ->paginate(15);

        return view('admin.juryMembers.index', compact(
            'juryMembers', 'submissionsCount'
        ));
    }

    /**
     * Store jury member.
     *
     * @param  StoreJuryMember $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreJuryMember $request)
    {
        JuryMember::create($request->validated());

        return redirect()->route('admin.juryMembers.index')
            ->with(['success' => 'Jury member created']);
    }

    /**
     * Delete jury member and associated reviews.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $juryMember = JuryMember::findOrFail($id, ['id']);

        Review::author($juryMember->id)->delete();

        $juryMember->delete();

        return back()->with(['success' => 'Jury member set as inactive']);
    }

    /**
     * Delete jury member and associated reviews.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $juryMember = JuryMember::onlyTrashed()->findOrFail($id, ['id']);

        Review::onlyTrashed()->author($juryMember->id)->restore();

        $juryMember->restore();

        return back()->with(['success' => 'Jury member set as active']);
    }
}
