<?php

namespace App\Http\Requests\JuryMembers;

use Illuminate\Foundation\Http\FormRequest;

class Review extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'points'    => 'required|string|numeric|min:1|max:10',
            'comments'  => 'required|string'
        ];
    }
}
