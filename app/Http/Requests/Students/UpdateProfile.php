<?php

namespace App\Http\Requests\Students;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'students.*.avatar'         => 'nullable|image|max:2048',
            'students.*.course'         => 'nullable|string|max:255',
            'students.*.year'           => 'nullable|string|digits:4',
            'students.*.contact'        => 'nullable|string|size:12',
            'students.*.registration'   => 'required|string|max:255'
        ];
    }
}
