<?php

namespace App\Http\Requests\Students;

use Illuminate\Foundation\Http\FormRequest;

class Register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'students.*.name'           => 'required|string|max:255',
            'students.*.nic'            => ['required', 'string', 'regex:/^([0-9]{9}[x|X|v|V]|[0-9]{12})$/'],
            'students.*.university'     => 'required|string|max:255',
            'students.*.registration'   => 'required|string|max:255',
            'students.*.course'         => 'nullable|string|max:255',
            'students.*.year'           => 'nullable|string|digits:4',
            'students.*.email'          => 'required|string|email|max:255',
            'students.*.contact'        => 'nullable|string|size:12',
            'students.*.education'      => 'required|string|max:255',
            'students.0.password'       => 'required|string|min:8|max:255|confirmed'
        ];
    }
}
