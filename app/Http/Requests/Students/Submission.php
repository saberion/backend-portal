<?php

namespace App\Http\Requests\Students;

use Illuminate\Foundation\Http\FormRequest;

class Submission extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'competition'   => 'required|string|digits:1|min:1|max:1',
            'terms'         => 'required|accepted',
            'proposal'      => 'required|file|mimes:pdf|max:20480',
            'design'        => 'required|file|mimes:pdf|max:20480',
        ];
    }
}
