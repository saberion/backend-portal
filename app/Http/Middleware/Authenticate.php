<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Arr;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * The array of guards.
     *
     * @var array
     */
    private $guards;

    /**
     * Override Middleware::handle().
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Closure $next
     * @param  string[] ...$guards
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $this->guards = $guards;

        return parent::handle($request, $next, ...$guards);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            $guard = Arr::first($this->guards);

            if ($guard === 'student') {
                return route('root');
            }

            if ($guard === 'juryMember') {
                return route('juryMembers.login');
            }

            if ($guard === 'admin') {
                return route('admin.login');
            }

            return route('login');
        }
    }
}
