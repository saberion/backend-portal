<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard('student')->check()) {
            return redirect()->route('students.profile.index');
        }

        if (Auth::guard('juryMember')->check()) {
            return redirect()
                ->route('juryMembers.dashboard.index', ['competition' => 1]);
        }

        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin.dashboard.index');
        }

        if (Auth::guard($guard)->check()) {
            return redirect(RouteServiceProvider::HOME);
        }

        return $next($request);
    }
}
