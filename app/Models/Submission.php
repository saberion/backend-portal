<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Submission extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['competition', 'proposal', 'design'];

    /**
     * Get the entry that owns the submission.
     */
    public function entry()
    {
        return $this->belongsTo('App\Models\Entry');
    }

    /**
     * Get the reviews of the submission.
     */
    public function reviews()
    {
        return $this->hasMany('App\Models\Review');
    }

    /**
     * Get the review of the authenticated jury member.
     */
    public function review()
    {
        return $this->hasOne('App\Models\Review')
            ->author(auth()->guard('juryMember')->id());
    }

    /**
     * Get the result of the submission.
     */
    public function result()
    {
        return $this->hasOne('App\Models\Result');
    }

    /**
     * Scope a query to only include the submission of the specified
     * competition.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $number
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCompetition($query, $number)
    {
        return $query->where(['competition' => $number]);
    }
}
