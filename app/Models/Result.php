<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['submission_id', 'average_points', 'rank'];

    /**
     * Get the submission that owns the result.
     */
    public function submission()
    {
        return $this->belongsTo('App\Models\Submission');
    }
}
